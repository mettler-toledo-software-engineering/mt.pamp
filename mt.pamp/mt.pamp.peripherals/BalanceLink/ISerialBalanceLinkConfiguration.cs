﻿
using MT.Singularity.Platform.Devices;
using MT.Singularity.Serialization;

namespace mt.pamp.peripherals.BalanceLink
{
    public interface ISerialBalanceLinkConfiguration
    {
        int CodePage { get; set; }
        int ResistanceClass { get; set; }
        ISerialInterface SerialInterface { get; set; }
        StringSerializer StringSerializer { get; set; }
        int BalanceLinkPort { get; set; }
    }
}
