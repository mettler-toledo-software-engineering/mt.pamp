﻿using log4net;
using MT.Singularity.Composition;
using MT.Singularity.IO;
using MT.Singularity.Logging;
using System;
using System.Threading.Tasks;

namespace mt.pamp.peripherals.BalanceLink
{
    [Export(typeof(IBalanceLink))]
    public class BalanceLink : IBalanceLink
    {
        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(BalanceLink);

        public ISerialBalanceLinkConfiguration BalanceLinkConfiguration { get; }

        public event EventHandler<BalanceLinkResult> BalanceLinkExecutedEvent;

        public BalanceLink(ISerialBalanceLinkConfiguration balanceLinkConfiguration)
        {
            BalanceLinkConfiguration = balanceLinkConfiguration;
        }
        private async Task WriteBufferToChannel(string line)
        {
            await BalanceLinkConfiguration.StringSerializer.WriteAsync($"{line}");
        }

        public async Task<bool> Send(string template)
        {
            try
            {
                await BalanceLinkConfiguration.StringSerializer.OpenAsync();
                await WriteBufferToChannel(template);
                await BalanceLinkConfiguration.StringSerializer.CloseAsync();

                BalanceLinkExecutedEvent?.Invoke(this, BalanceLinkResult.SendOk);

                return true;
            }
            catch (Exception e)
            {
                Logger.ErrorEx("sending exception", SourceClass, e);
                BalanceLinkExecutedEvent?.Invoke(this, BalanceLinkResult.SerialChannelException);

                return false;
            }
            finally {
                await BalanceLinkConfiguration.StringSerializer.CloseAsync();
            }
        }
    }
}
