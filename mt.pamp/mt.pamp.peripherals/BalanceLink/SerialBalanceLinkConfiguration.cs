﻿using MT.Singularity.Platform.Devices;
using MT.Singularity.Serialization;

namespace mt.pamp.peripherals.BalanceLink
{
    public class SerialBalanceLinkConfiguration : ISerialBalanceLinkConfiguration
    {
        public int CodePage { get; set; }

        public int ResistanceClass { get; set; }

        public int BalanceLinkPort { get; set; }

        public ISerialInterface SerialInterface { get; set; }

        public StringSerializer StringSerializer { get; set; }
    }
}
