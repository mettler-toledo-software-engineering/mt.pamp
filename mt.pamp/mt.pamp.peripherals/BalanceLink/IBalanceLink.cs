﻿
using System;
using System.Threading.Tasks;

namespace mt.pamp.peripherals.BalanceLink
{
    public enum BalanceLinkResult
    {
        SendOk,
        SerialChannelException
    }

    public interface IBalanceLink
    {
        event EventHandler<BalanceLinkResult> BalanceLinkExecutedEvent;
        ISerialBalanceLinkConfiguration BalanceLinkConfiguration { get; }
        Task<bool> Send(string template);
    }
}
