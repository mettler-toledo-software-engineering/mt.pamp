﻿using System;

namespace mt.pamp.peripherals.Usb
{
    public interface IUsbEventProvider
    {
        event Action<string> DeviceInsertedEvent;
        event Action DeviceRemovedEvent;
        void RegisterForUsbEvents();
    }
}