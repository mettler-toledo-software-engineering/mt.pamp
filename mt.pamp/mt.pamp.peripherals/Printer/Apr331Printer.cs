﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using MT.Singularity.Composition;
using MT.Singularity.IO;
using MT.Singularity.Logging;

namespace mt.pamp.peripherals.Printer
{
    [Export(typeof(IApr331Printer))]
    public class Apr331Printer : IApr331Printer
    {

        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(Apr331Printer);
        public ISerialPrinterConfiguration  PrinterConfiguration { get; }

        public event EventHandler<PrintResult> PrintExecutedEvent;
        public event Action ResetPrinterInfosEvent;

        public Apr331Printer(ISerialPrinterConfiguration printerConfiguration)
        {
            PrinterConfiguration = printerConfiguration;
            ResetPrinterInfosEvent?.Invoke();
        }

        public async Task Print(List<string> template)
        {
            try
            {
                await PrinterConfiguration.StringSerializer.OpenAsync();
                await WriteBufferToChannel(template);
                await PrinterConfiguration.StringSerializer.CloseAsync();

                PrintExecutedEvent?.Invoke(this, PrintResult.PrintOk);
            }
            catch (Exception e)
            {
                Logger.ErrorEx("printing exception", SourceClass, e);
                PrintExecutedEvent?.Invoke(this, PrintResult.SerialChannelException);
            }
            finally
            {
                await PrinterConfiguration.StringSerializer.CloseAsync();
            }
        }

        private async Task WriteBufferToChannel(List<string> template)
        {
            foreach (string line in template)
            {
                await PrinterConfiguration.StringSerializer.WriteAsync($"{line}");
                Thread.Sleep(PrinterConfiguration.TimeDelayBetweenPrintingLines);
            }
        }
    }
}
