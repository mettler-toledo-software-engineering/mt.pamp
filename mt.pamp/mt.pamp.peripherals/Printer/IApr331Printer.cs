﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace mt.pamp.peripherals.Printer
{
    public enum PrintResult
    {
        PrintOk,
        SerialChannelException
    }

    public interface IApr331Printer
    {
        event EventHandler<PrintResult> PrintExecutedEvent;
        ISerialPrinterConfiguration PrinterConfiguration { get; }
        Task Print(List<string> template);
        event Action ResetPrinterInfosEvent;
    }
}
