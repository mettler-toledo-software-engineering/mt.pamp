﻿using MT.Singularity.Composition;
using MT.Singularity.Platform.Devices;
using MT.Singularity.Serialization;

namespace mt.pamp.peripherals.Camera
{
    [Export(typeof(ISerialCameraConfiguration))]
    [InjectionBehavior(IsSingleton = true)]
    public class SerialCameraConfiguration : ISerialCameraConfiguration
    {
        public int CodePage { get; set; }

        public int ResistanceClass { get; set; }

        public int CameraPort { get; set; }

        public ISerialInterface SerialInterface { get; set; }

        public StringSerializer StringSerializer { get; set; }
    }
}
