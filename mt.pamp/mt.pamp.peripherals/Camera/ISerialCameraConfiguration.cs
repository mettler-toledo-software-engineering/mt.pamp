﻿using MT.Singularity.Platform.Devices;
using MT.Singularity.Serialization;

namespace mt.pamp.peripherals.Camera
{
    public interface ISerialCameraConfiguration
    {
        int CodePage { get; set; }
        int ResistanceClass { get; set; }
        ISerialInterface SerialInterface { get; set; }
        StringSerializer StringSerializer { get; set; }
        int CameraPort { get; set; }
    }
}