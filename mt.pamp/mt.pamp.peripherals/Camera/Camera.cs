﻿using System;
using System.Threading.Tasks;
using log4net;
using MT.Singularity.Composition;
using MT.Singularity.IO;
using MT.Singularity.Logging;

namespace mt.pamp.peripherals.Camera
{
    [Export(typeof(ICamera))]
    public class Camera : ICamera
    {
        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(Camera);

        public ISerialCameraConfiguration CameraConfiguration { get; }

        public event EventHandler<CameraResult> CameraExecutedEvent;


        public Camera(ISerialCameraConfiguration cameraConfiguration)
        {
            CameraConfiguration = cameraConfiguration;
        }
        
        private async Task WriteBufferToChannel(string line)
        {
            await CameraConfiguration.StringSerializer.WriteAsync($"{line}");
        }

        public async Task<bool> Send(string template)
        {
            try
            {
                await CameraConfiguration.StringSerializer.OpenAsync();
                await WriteBufferToChannel(template);
                await CameraConfiguration.StringSerializer.CloseAsync();

                CameraExecutedEvent?.Invoke(this, CameraResult.SendOk);

                return true;
            }
            catch (Exception e)
            {
                Logger.ErrorEx("sending exception", SourceClass, e);
                CameraExecutedEvent?.Invoke(this, CameraResult.SerialChannelException);

                return false;
            }
            finally
            {
                await CameraConfiguration.StringSerializer.CloseAsync();
            }
        }
    }
}
