﻿using System;
using System.Threading.Tasks;

namespace mt.pamp.peripherals.Camera
{
    public enum CameraResult
    {
        SendOk,
        SerialChannelException
    }

    public interface ICamera
    {
        event EventHandler<CameraResult> CameraExecutedEvent;
        ISerialCameraConfiguration CameraConfiguration { get; }
        Task<bool> Send(string template);
    }
}
