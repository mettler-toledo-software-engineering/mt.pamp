﻿namespace mt.pamp.persistance
{
    public interface IDbInitializer
    {
        void Initialize();
    }
}