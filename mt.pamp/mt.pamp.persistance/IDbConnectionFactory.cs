﻿using System.Data;

namespace mt.pamp.persistance
{
    public interface IDbConnectionFactory
    {
        IDbConnection Connect();
    }
}