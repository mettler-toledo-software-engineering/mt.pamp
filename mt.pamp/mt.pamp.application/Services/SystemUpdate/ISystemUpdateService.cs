﻿namespace mt.pamp.application.Services.SystemUpdate
{
    public interface ISystemUpdateService
    {
        bool ExecuteUpdate();
    }
}