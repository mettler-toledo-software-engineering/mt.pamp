﻿using System.Collections.Generic;
using mt.pamp.application.Models;
using mt.pamp.application.Services.HouseKeeping;
using System;

namespace mt.pamp.application.Services.Printing
{
    public class PrintTemplate : ICreatePrintTemplateService
    {
        private const string Height2 = "\x1B\x48" + "2";
        private const string Height3 = "\x1B\x48" + "3";
        private const string DoubleWidth = "\x0E";
        private const string SingleWidth = "\x0F";

        private const int MaxLine = 48;

        private List<string> _template = new List<string>();

        public List<string> CreateTemplate(Measurement measurement, string barcode, string customerNumber)
        {
            _template.Clear();

            SetSummarizingBody(measurement);
            SetTitleBody();
            SetDateTimeBody(measurement.StartTime, measurement.StartTime);
            SetCustomerBody(barcode, customerNumber);
            SetWeighingBody(measurement.ToWeighing());
            SetCountingBody(measurement.Weighings.Count);

            return _template;
        }

        public List<string> CreateTemplate(Weighing weighing)
        {
            _template.Clear();

            DateTime now = DateTime.Now;
            
            SetTitleBody();
            SetDateTimeBody(now, now);
            SetWeighingBody(weighing);

            return _template;
        }
    
        #region SetTitleBody    

        private void SetTitleBody()
        {
            AddLine(SingleWidth + Height2, string.Empty);
            PrintCharLine('*');
            AddLine(Height3, string.Empty);
            AddLine_Center(DoubleWidth, "MKS PAMP");
            AddLine_Center(SingleWidth, "Tesoro");
            AddLine(SingleWidth + Height2, string.Empty);
            PrintCharLine('*');
        }

        #endregion

        #region SetDateTimeBody

        private void SetDateTimeBody(DateTime date, DateTime time)
        {
            AddLine(string.Empty, " ");
            AddLine_LeftRight(string.Empty, "Data", date.ToFormattedDate());
            AddLine_LeftRight(string.Empty, "Ora", time.ToFormattedTime());
        }

        #endregion

        #region SetCustomerBody

        private void SetCustomerBody(string barcode, string customerNumber)
        {
            AddLine(DoubleWidth + Height3, string.Empty);
            AddLine_LeftRight(string.Empty, "Barcode", barcode);
            AddLine_LeftRight(string.Empty, "Numero di clienti", customerNumber);
        }

        #endregion

        #region SetSummarizingBody

        private void SetSummarizingBody(Measurement m)
        {
            int counter = 0;

            foreach (Weighing weighing in m.Weighings)
            {
                SetWeighingBody(weighing);
                SetCountingBody(++counter);
                PrintCharLine('*');
            }
        }

        #endregion

        #region SetWeighingBody

        private void SetWeighingBody(Weighing weighing)
        {
            AddLine(SingleWidth + Height2, string.Empty);
            AddLine_LeftRight(string.Empty, "Lordo", $"{weighing.GrossWeight.ToFormattedWeight()}");
            AddLine_LeftRight(string.Empty, "Netto", $"{weighing.NetWeight.ToFormattedWeight()}");
            AddLine_LeftRight(string.Empty, "Tara", $"{weighing.TareWeight.ToFormattedWeight()}");
            AddLine(string.Empty, " ");
        }

        #endregion

        #region SetCountingBody

        private void SetCountingBody(int counter)
        {
            AddLine_LeftRight(string.Empty, "n", $"{counter}");
            AddLine(string.Empty, " ");
        }

        #endregion

        #region Add lines to template

        #region PrintCharLine

        private void PrintCharLine(char c)
        {
            _template.Add("".PadRight(MaxLine, c));
        }

        #endregion

        #region AddLine_LeftRight

        private void AddLine_LeftRight(string formatting, string textL, string textR)
        {
            int padding = MaxLine - textL.Length - textR.Length;
            _template.Add(textL + "".PadRight(padding, ' ') + textR);
        }

        #endregion

        #region AddLine_Center

        private void AddLine_Center(string formatting, string textC)
        {
            int padding = (MaxLine - textC.Length) / 2;
            string filling = "".PadRight(padding, ' ');
            _template.Add(filling + textC + filling);
        }

        #endregion

        #region AddLine

        private void AddLine(string formatting, string item)
        {
            _template.Add(formatting + item);
        }

        #endregion

        #endregion
    }
}
