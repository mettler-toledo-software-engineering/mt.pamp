﻿using mt.pamp.application.Models;
using System.Collections.Generic;

namespace mt.pamp.application.Services.Printing
{
    public interface ICreatePrintTemplateService
    {
        List<string> CreateTemplate(Measurement measurement, string barcode, string customerNumber);
        List<string> CreateTemplate(Weighing weighing);
    }
}