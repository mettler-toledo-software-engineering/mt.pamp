﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace mt.pamp.application.Services.Printing
{
    public interface ICreatePdfTemplateService
    {
        Task<string> CreatePdfDocument(List<string> linesToPrint, string charge);
    }
}