﻿using mt.pamp.application.Models;
using System;

namespace mt.pamp.application.Services.HouseKeeping
{
    public static class ExtensionMethods
    {
        private const string WeightFormat = "{0:0.(0) g}";
        private const string DateFormat = "dd.MM.yy";
        private const string TimeFormat = "HH:mm:ss";

        public static string ToFormattedWeight(this double weight)
        {
            return string.Format(WeightFormat, weight);
        }

        public static string ToFormattedDate(this DateTime date)
        {
            return date.ToString(DateFormat);
        }

        public static string ToFormattedTime(this DateTime time)
        {
            return time.ToString(TimeFormat);
        }

        public static Weighing ToWeighing(this Measurement m)
        {
            return new Weighing()
            {
                GrossWeight = m.GrossWeight(),
                NetWeight = m.NetWeight(),
                TareWeight = m.TareWeight()
            };
        }
    }
}
