﻿namespace mt.pamp.application.Models
{
    public enum MessageColors
    {
        Red,
        Yellow,
        Green,
        LightBlue
    }

    public enum Messages
    {
        Empty,
        NoWeight,
        RemoveWeight,
        SendWeightToCamera,
        PrintLabel,
        ProcessFinished,
        ErrorWhileSendingToCamera,
        ErrorWhilePrintingLabel,
        ErrorWhileSendingToBalanceLink,
        InvalidOrderInformation
    }
}
