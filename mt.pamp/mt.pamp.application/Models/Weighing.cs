﻿using MT.Singularity.Platform.Devices.Scale;

namespace mt.pamp.application.Models
{
    public class Weighing : DomainObject
    {
        public double GrossWeight { get; set; }
        public double NetWeight { get; set; }
        public double TareWeight { get; set; }

        public Weighing()
        {
            GrossWeight = 0;
            NetWeight = 0;
            TareWeight = 0;
        }

        public Weighing(WeightInformation weight)
            : base()
        {
            if (weight != null)
            {
                GrossWeight = weight.GrossWeight;
                NetWeight = weight.NetWeight;
                TareWeight = weight.TareWeight;
            }
        }
    }
}
