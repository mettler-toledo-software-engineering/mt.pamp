﻿namespace mt.pamp.application.Models
{
    public interface IUser
    {
        string UserName { get; set; }
    }
}