﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace mt.pamp.application.Models
{
    public class Measurement : DomainObject
    {
        public List<Weighing> Weighings { get; set; }
        public DateTime StartTime { get; set; }
        
        public double GrossWeight()
        {
            return Weighings.Sum(w => w.GrossWeight);
        }

        public double NetWeight()
        {
            return Weighings.Sum(w => w.NetWeight);
        }

        public double TareWeight()
        {
            return Weighings.Sum(w => w.TareWeight);
        }
    }
}
