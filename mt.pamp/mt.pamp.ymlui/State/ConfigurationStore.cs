﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using mt.pamp.ymlui.Configuration;

namespace mt.pamp.ymlui.State
{
    public class ConfigurationStore
    {

        public event Action<PampConfiguration> ConfigurationChanged;
        public ConfigurationStore(PampConfiguration configuration)
        {
            CurrentConfiguration = configuration;
            CurrentConfiguration.PropertyChanged += CurrentConfigurationOnPropertyChanged;
        }

        private void CurrentConfigurationOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            ConfigurationChanged?.Invoke(CurrentConfiguration);
        }

        public PampConfiguration CurrentConfiguration { get; }
    }
}
