﻿using MT.Singularity.Composition;
using MT.Singularity.Platform.Devices.Scale;

namespace mt.pamp.ymlui.State
{
    [InjectionBehavior(IsSingleton = true)]
    [Export(typeof(WeighingProcessStore))]
    public class WeighingProcessStore
    {
        private readonly MessageStore _messageStore;

        public WeighingProcessStore(MessageStore messageStore, IScaleService scaleService)
        {
            _messageStore = messageStore;        
        }
    }
}
