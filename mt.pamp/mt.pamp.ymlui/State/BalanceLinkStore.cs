﻿
using log4net;
using mt.pamp.peripherals.BalanceLink;
using mt.pamp.ymlui.Configuration;
using mt.pamp.ymlui.Exceptions;
using mt.pamp.ymlui.Infrastructure;
using MT.Singularity.Logging;
using System;
using System.Threading.Tasks;

namespace mt.pamp.ymlui.State
{
    public class BalanceLinkStore
    {
        public event Action BalanceLinkChanged;

        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(BalanceLinkStore);

        private BalanceLink _currentBalanceLink;
        private readonly PampConfiguration _pampConfiguration;
        public bool InitializeBalanceLinkSuccess { get; set; }

        public BalanceLinkStore(ConfigurationStore pampConfiguration, IBalanceLinkFactoryService balanceLinkFactory)
        {
            _pampConfiguration = pampConfiguration.CurrentConfiguration;

            balanceLinkFactory.BalanceLinkChanged += BalanceLinkFactory_BalanceLinkChanged;
            balanceLinkFactory.InitializeBalanceLinkEvent += BalanceLinkFactory_InitializeBalanceLinkEvent; ;
            balanceLinkFactory.CreateBalanceLink().ContinueWith(HandleException);
        }

        private void BalanceLinkFactory_InitializeBalanceLinkEvent(bool success)
        {
            InitializeBalanceLinkSuccess = success;
        }

        private void HandleException(Task<BalanceLink> task)
        {
            InitializeBalanceLinkSuccess = task.Exception == null;

            if (!InitializeBalanceLinkSuccess)
            {
                Logger.ErrorEx(task.Exception.Message, SourceClass, task.Exception);
            }
            else
            {
                _currentBalanceLink = task.Result;
            }
        }

        private void BalanceLinkFactory_BalanceLinkChanged(BalanceLink obj)
        {
            _currentBalanceLink = obj;

            BalanceLinkChanged?.Invoke();
        }

        public async Task SendCurrentWeight(double weight)
        {
            bool res = await _currentBalanceLink.Send(weight.ToString());

            if (!res)
            {
                throw new SendToBalanceLinkException();
            }
        }
    }
}
