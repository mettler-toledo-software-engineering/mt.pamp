﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using mt.pamp.application.Models;
using mt.pamp.application.Services.DataAccess;
using MT.Singularity.Composition;

namespace mt.pamp.ymlui.State
{
    [InjectionBehavior(IsSingleton = true)]
    public class MeasurementStore
    {
        private readonly IMeasurementDataAccess _measurementData;
        public event Action<Measurement> MeasurementCreated;
        

        public MeasurementStore(IMeasurementDataAccess measurementdata)
        {
            _measurementData = measurementdata;
        }


        
        
        public async Task CreateMeasurement(Measurement measurement)
        {
            var result = await _measurementData.CreateMeasurement(measurement);

            MeasurementCreated?.Invoke(result);
        }


        public async Task DeleteMeasurementsByOrderId(int orderId)
        {
            var result = await _measurementData.DeleteAllMeasurementsByOrder(orderId);

        }

        public async Task DeleteAllMeasurements(int orderId)
        {
            var result = await _measurementData.DeleteAllMeasureMents();
        }


    }
}
