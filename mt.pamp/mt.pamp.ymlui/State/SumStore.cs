﻿using mt.pamp.application.Models;
using MT.Singularity.Composition;
using MT.Singularity.Platform.Devices.Scale;
using System;
using System.Collections.Generic;

namespace mt.pamp.ymlui.State
{
    [InjectionBehavior(IsSingleton = true)]
    [Export(typeof(SumStore))]
    public class SumStore
    {
        public event Action<Measurement> UpdateMeasurement;
        public event Action NewCurrentWeight;
        public event Action CompleteStateChanged;
        
        public double TotalWeight { get; set; }
        public int SumCounter { get; set; }
        public bool IsCompleting { get; private set; }

        public bool PrintSucceeded { get; set; }
        public bool SendToCameraSucceed { get; set; }
        public bool BalanceLinkSuceeded { get; set; }

        public Measurement CurrentMeasurement { get; set; }
        public Weighing CurrentWeighing { get; set; }
        public WeightInformation CurrentWeightInformation { get; private set; }

        public string Barcode { get; set; }
        public string CustomerNumber { get; set; }
                

        private readonly IScaleService _scaleService;

        public SumStore(IScaleService scaleService)
        {
            _scaleService = scaleService;

            _scaleService.SelectedScale.NewWeightInDisplayUnit += SelectedScale_NewWeightInDisplayUnit;
            _scaleService.SelectedScale.NewChangedWeightInDisplayUnit += SelectedScale_NewChangedWeightInDisplayUnit;

            ResetValues();
        }

        private void SelectedScale_NewWeightInDisplayUnit(WeightInformation weight)
        {
            CurrentWeightInformation = weight;

            CurrentWeighing = new Weighing(weight);

            NewCurrentWeight?.Invoke();
        }

        private void SelectedScale_NewChangedWeightInDisplayUnit(WeightInformation weight)
        {
            _scaleService.SelectedScale.NewWeightInDisplayUnit -= SelectedScale_NewWeightInDisplayUnit;

            CurrentWeightInformation = weight;
            CurrentWeighing = new Weighing(weight);

            NewCurrentWeight?.Invoke();            
        }
        
        public void ResetValues()
        {
            CurrentMeasurement = new Measurement();
            CurrentMeasurement.StartTime = DateTime.Now;
            CurrentMeasurement.Weighings = new List<Weighing>();

            TotalWeight = 0;
            SumCounter = 0;
            IsCompleting = false;

            SelectedScale_NewWeightInDisplayUnit(CurrentWeightInformation);
            UpdateMeasurement?.Invoke(CurrentMeasurement);
            CompleteStateChanged?.Invoke();
        }

        public void CompleteMeasurement(bool complete = true, bool printSuccess = true, bool cameraSuccess = true, bool balanceLinkSuccess = true)
        {
            IsCompleting = complete;
            PrintSucceeded = printSuccess;
            SendToCameraSucceed = cameraSuccess;
            BalanceLinkSuceeded = balanceLinkSuccess;

            CompleteStateChanged?.Invoke();
        }
                
        public void AddWeightToTotalWeight()
        {
            CurrentMeasurement.Weighings.Add(CurrentWeighing);

            TotalWeight = CurrentMeasurement.NetWeight();
            SumCounter = CurrentMeasurement.Weighings.Count;
            
            UpdateMeasurement?.Invoke(CurrentMeasurement);
        }
    }
}
