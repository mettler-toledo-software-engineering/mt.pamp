﻿using System.Threading.Tasks;

using log4net;
using mt.pamp.peripherals.Printer;
using mt.pamp.ymlui.Configuration;
using mt.pamp.ymlui.Infrastructure;
using MT.Singularity.Logging;
using mt.pamp.application.Services.Printing;
using mt.pamp.application.Models;
using System;

namespace mt.pamp.ymlui.State
{
    public class PrinterStore
    {
        public event Action PrinterChanged;

        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(PrinterStore);

        private IApr331Printer _currentPrinter;
        private readonly PampConfiguration _pampConfiguration;
        private readonly ICreatePrintTemplateService _printTemplateService;
        private readonly IPrinterFactoryService _printerFactory;
        
        public bool InitializePrinterSuccess { get; set; }

        public PrinterStore(ConfigurationStore pampConfiguration, IPrinterFactoryService printerFactory, ICreatePrintTemplateService printTemplateService)
        {
            _pampConfiguration = pampConfiguration.CurrentConfiguration;
            _printerFactory = printerFactory;
            _printTemplateService = printTemplateService;
            
            printerFactory.PrinterChanged += PrinterFactoryOnPrinterChanged;
            printerFactory.InitializePrinterEvent += PrinterFactory_InitializePrinterEvent;
            printerFactory.CreateApr331Printer().ContinueWith(HandleException);
        }

        private void PrinterFactory_InitializePrinterEvent(bool success)
        {
            InitializePrinterSuccess = success;
        }

        private void HandleException(Task<IApr331Printer> task)
        {
            InitializePrinterSuccess = task.Exception == null;

            if (!InitializePrinterSuccess)
            {
                Logger.ErrorEx(task.Exception.Message, SourceClass, task.Exception);
            }
            else
            {
                _currentPrinter = task.Result;
            }
        }
        
        private void PrinterFactoryOnPrinterChanged(IApr331Printer obj)
        {
            _currentPrinter = obj;

            PrinterChanged?.Invoke();
        }
        
        public async Task PrintLabel(Measurement measurement, string barcode, string customerNumber)
        {
            var template = _printTemplateService.CreateTemplate(measurement, barcode, customerNumber);

            await _currentPrinter.Print(template);
        }

        public async Task PrintLabel(Weighing weighing)
        {
            var template = _printTemplateService.CreateTemplate(weighing);

            await _currentPrinter.Print(template);
        }
    }
}
