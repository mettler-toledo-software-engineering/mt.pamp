﻿using log4net;
using mt.pamp.application.Models;
using mt.pamp.application.Services.HouseKeeping;
using mt.pamp.peripherals.Camera;
using mt.pamp.ymlui.Configuration;
using mt.pamp.ymlui.Exceptions;
using mt.pamp.ymlui.Infrastructure;
using MT.Singularity.Logging;
using System;
using System.Threading.Tasks;

namespace mt.pamp.ymlui.State
{
    public class CameraStore
    {
        public event Action CameraChanged;

        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(CameraStore);

        private Camera _currentCamera;
        private readonly PampConfiguration _pampConfiguration;

        public bool InitializeCameraSuccess { get; set; }

        public CameraStore(ConfigurationStore pampConfiguration, ICameraFactoryService cameraFactory)
        {
            _pampConfiguration = pampConfiguration.CurrentConfiguration;
            
            cameraFactory.CameraChanged += CameraFactory_CameraChanged;
            cameraFactory.InitializeCameraEvent += CameraFactory_InitializeCameraEvent;
            cameraFactory.CreateCamera().ContinueWith(HandleException);
        }

        private void CameraFactory_InitializeCameraEvent(bool success)
        {
            InitializeCameraSuccess = success;
        }

        private void HandleException(Task<Camera> task)
        {
            InitializeCameraSuccess = task.Exception == null;

            if (!InitializeCameraSuccess)
            {
                Logger.ErrorEx(task.Exception.Message, SourceClass, task.Exception);
            }
            else
            {
                _currentCamera = task.Result;
            }
        }

        private void CameraFactory_CameraChanged(Camera obj)
        {
            _currentCamera = obj;

            CameraChanged?.Invoke();
        }

        public async Task SendTotalWeight(string barcode, string customerNumber, Measurement m)
        {
            string template = GetTemplate(
                barcode,
                customerNumber,
                m.GrossWeight().ToFormattedWeight(),
                m.NetWeight().ToFormattedWeight(),
                m.TareWeight().ToFormattedWeight()
                );

            bool res = await _currentCamera.Send(template);

            if (!res)
            {
                throw new SendToCameraFailedException();
            }
        }

        public async Task SendCurrentWeight(string barcode, string customerNumber, Weighing w)
        {
            string template = GetTemplate(
                barcode,
                customerNumber,
                w.GrossWeight.ToFormattedWeight(),
                w.NetWeight.ToFormattedWeight(),
                w.TareWeight.ToFormattedWeight()
                );
                
            bool res = await _currentCamera.Send(template);

            if (!res)
            {
                throw new SendToCameraFailedException();
            }
        }

        public async Task SendEmptyString()
        {
            bool res = await _currentCamera.Send(" ");

            if (!res)
            {
                throw new SendToCameraFailedException();
            }
        }

        #region Template Helper Method

        private string GetTemplate(string barcode, string customerNumber, string gross, string net, string tare)
        {
            DateTime now = DateTime.Now;
            string dateTime = $"{now.ToFormattedDate()} {now.ToFormattedTime()}";

            return $"{dateTime}, {barcode}, {customerNumber}, L: {gross}, N: {net}, T: {tare}";
        }

        #endregion
    }
}
