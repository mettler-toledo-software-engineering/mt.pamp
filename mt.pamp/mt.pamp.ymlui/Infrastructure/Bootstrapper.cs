﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using log4net;
using mt.pamp.application.Services.Printing;
using mt.pamp.application.Services.SystemUpdate;
using mt.pamp.peripherals.Usb;
using mt.pamp.persistance;
using mt.pamp.ymlui.Configuration;
using mt.pamp.ymlui.State;
using mt.pamp.ymlui.ViewModels;
using mt.pamp.ymlui.Views;
// ReSharper disable once RedundantUsingDirective
using MT.Singularity.Composition;
using MT.Singularity.Logging;
using MT.Singularity.Platform.CommonUX.Infrastructure;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Platform.Devices.Scale;
using MT.Singularity.Platform.Infrastructure;

namespace mt.pamp.ymlui.Infrastructure
{
    /// <summary>
    /// Initializes and runs the application.
    /// </summary>
    internal class Bootstrapper : ClientBootstrapperBase
    {
        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(Bootstrapper);
        /// <summary>
        /// Initializes a new instance of the <see cref="Bootstrapper"/> class.
        /// </summary>
        public Bootstrapper()
            : base(Assembly.GetExecutingAssembly())
        {
        }

        /// <summary>
        /// Gets a value indicating whether it need to create customer database.
        /// </summary>
        protected override bool NeedToCreateCustomerDatabase
        {
            get { return true; }
        }

        protected override async void InitializeApplication()
        {
            await InitializeCustomerService();
            base.InitializeApplication();
        }

        private async Task InitializeCustomerService()
        {
            try
            {
                CompositionContainer.EnableRecursiveResolution();
                //configuration
                var engine = CompositionContainer.Resolve<IPlatformEngine>();
                var securityService = await engine.GetSecurityServiceAsync();
                var configurationStore = CompositionContainer.Resolve<IConfigurationStore>();
                var geistlichComponent = new PampComponents(configurationStore, securityService, CompositionContainer);
                CompositionContainer.AddInstance<IPampComponents>(geistlichComponent);
                CompositionContainer.AddInstance<ISystemUpdateService>(new SystemUpdateService());
                Logger.InfoEx("configuration successfully initialized", SourceClass);

                //database
                //string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["sqlite"].ConnectionString;
                //CompositionContainer.AddInstance<IDbConnectionFactory>(new SqliteDbConnectionFactory(connectionstring));
                //CompositionContainer.AddInstance<IDbInitializer>(new SqliteDbInitializer(CompositionContainer.Resolve<IDbConnectionFactory>()));
                
                //IDbInitializer dbInitializer = CompositionContainer.Resolve<IDbInitializer>();
                //dbInitializer.Initialize();

                PampConfiguration configuration = geistlichComponent.GetConfigurationAsync().Result;
                State.ConfigurationStore myConfigurationStore = new State.ConfigurationStore(configuration);
                Logger.InfoEx("database successfully initialized", SourceClass);

                //services
                CompositionContainer.AddInstance<IPrinterFactoryService>(new PrinterFactory(engine, myConfigurationStore));
                CompositionContainer.AddInstance<ICameraFactoryService>(new CameraFactory(engine, myConfigurationStore));
                CompositionContainer.AddInstance<IBalanceLinkFactoryService>(new BalanceLinkFactory(engine, myConfigurationStore));
                CompositionContainer.AddInstance<ICreatePrintTemplateService>(new PrintTemplate());
                CompositionContainer.AddInstance<IUsbEventProvider>(new UsbEventProvider());
                Logger.InfoEx("services successfully initialized", SourceClass);

                //stores
                CompositionContainer.AddInstance(new PrinterStore(myConfigurationStore,
                    CompositionContainer.Resolve<IPrinterFactoryService>(),
                    CompositionContainer.Resolve<ICreatePrintTemplateService>()));
                CompositionContainer.AddInstance(new MessageStore());
                Logger.InfoEx("stores successfully initialized", SourceClass);

                //viewmodels
                CompositionContainer.AddInstance(new HomeScreenViewModel(
                    CompositionContainer.Resolve<MessageStore>(),
                    CompositionContainer.Resolve<SumStore>(),
                    CompositionContainer.Resolve<PrinterStore>(),
                    CompositionContainer.Resolve<BalanceLinkStore>()
                    ));
                CompositionContainer.AddInstance(new WeightButtonsViewModel(CompositionContainer.Resolve<IScaleService>()));
                CompositionContainer.AddInstance(new MessageWindowViewModel(CompositionContainer.Resolve<MessageStore>()));
                CompositionContainer.AddInstance(new WeightWindowViewModel(CompositionContainer.Resolve<MessageStore>(), CompositionContainer.Resolve<IScaleService>()));
                Logger.InfoEx("view models successfully initialized", SourceClass);

                //controls
                CompositionContainer.AddInstance(new WeightButtonsControl(CompositionContainer.Resolve<WeightButtonsViewModel>()));
                CompositionContainer.AddInstance(new WeightWindowControl(CompositionContainer.Resolve<WeightWindowViewModel>()));
                CompositionContainer.AddInstance(new MessageWindowControl(CompositionContainer.Resolve<MessageWindowViewModel>()));
                Logger.InfoEx("controls successfully initialized", SourceClass);

                //views
                CompositionContainer.AddFactory(() => new SumViewModel(
                    CompositionContainer.Resolve<MessageStore>(),
                    CompositionContainer.Resolve<PrinterStore>(),
                    CompositionContainer.Resolve<SumStore>(),
                    CompositionContainer.Resolve<CameraStore>(),
                    CompositionContainer.Resolve<BalanceLinkStore>()
                    ));
                CompositionContainer.AddFactory(() => new SumView(
                    CompositionContainer.Resolve<SumViewModel>(),
                    CompositionContainer.Resolve<WeightWindowControl>(),
                    CompositionContainer.Resolve<MessageWindowControl>()
                    ));
                Logger.InfoEx("views successfully initialized", SourceClass);
                Logger.InfoEx("initialization completed", SourceClass);
            }
            catch (Exception e)
            {
                Logger.ErrorEx(e.Message, SourceClass, e);
            }
        }
    }
}
