﻿using mt.pamp.peripherals.BalanceLink;
using System;

using System.Threading.Tasks;

namespace mt.pamp.ymlui.Infrastructure
{
    public interface IBalanceLinkFactoryService
    {
        event Action<BalanceLink> BalanceLinkChanged;
        event Action<bool> InitializeBalanceLinkEvent;
        Task<BalanceLink> CreateBalanceLink();
    }
}
