﻿using mt.pamp.ymlui.State;
using System;
using System.Linq;
using System.Threading.Tasks;
using MT.Singularity.Platform.Infrastructure;
using log4net;
using mt.pamp.ymlui.Configuration;
using MT.Singularity.IO;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Devices;
using MT.Singularity.Serialization;
using mt.pamp.peripherals.Camera;

namespace mt.pamp.ymlui.Infrastructure
{
    public class CameraFactory : ICameraFactoryService
    {
        private readonly ConfigurationStore _configurationStore;
        private readonly IPlatformEngine _platformEngine;
        public event Action<Camera> CameraChanged;
        public event Action<bool> InitializeCameraEvent;

        private Camera _currentCamera;

        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(CameraFactory);

        public CameraFactory(IPlatformEngine platformEngine, ConfigurationStore configurationStore)
        {
            _platformEngine = platformEngine;
            _configurationStore = configurationStore;
            _configurationStore.ConfigurationChanged += ConfigurationStoreOnConfigurationChanged;

            CreateCamera().ContinueWith(HandleException);
        }

        private void ConfigurationStoreOnConfigurationChanged(PampConfiguration configuration)
        {
            if (_currentCamera?.CameraConfiguration.CameraPort != configuration.CameraPort)
            {
                CreateCamera().ContinueWith(HandleException);
            }
        }

        private void HandleException(Task<Camera> obj)
        {
            if (obj.Exception != null)
            {
                Logger.ErrorEx("Camera could not be initialized", SourceClass, obj.Exception);
                InitializeCameraEvent?.Invoke(false);
                return;
            }

            _currentCamera = obj.Result;
            CameraChanged?.Invoke(_currentCamera);
            InitializeCameraEvent?.Invoke(true);
        }
        
        public async Task<Camera> CreateCamera()
        {
            var interfaceService = await _platformEngine.GetInterfaceServiceAsync();
            var allInterfaces = await interfaceService.GetAllInterfacesAsync();
            var allSerialInterfaces = allInterfaces.OfType<ISerialInterface>().ToList();
            var serialInterface = allSerialInterfaces.FirstOrDefault(serial => serial.LogicalPort == _configurationStore.CurrentConfiguration.CameraPort);

            IConnectionChannel<DataSegment> serialConnectionChannel = await serialInterface.CreateConnectionChannelAsync();
            var delimiterSerializer = new DelimiterSerializer(serialConnectionChannel, CommonDataSegments.Newline);
            var serializer = new StringSerializer(delimiterSerializer);
            serializer.CodePage = CodePage.CP1250;
            var cameraConfiguration = new SerialCameraConfiguration();
            cameraConfiguration.StringSerializer = serializer;
            cameraConfiguration.CameraPort = _configurationStore.CurrentConfiguration.CameraPort;

            Camera camera = new Camera(cameraConfiguration);

            return camera;
        }
    }
}
