﻿using System;
using System.Threading.Tasks;
using mt.pamp.peripherals.Printer;

namespace mt.pamp.ymlui.Infrastructure
{
    public interface IPrinterFactoryService
    {
        event Action<IApr331Printer> PrinterChanged;
        event Action<bool> InitializePrinterEvent;
        Task<IApr331Printer> CreateApr331Printer();
    }
}