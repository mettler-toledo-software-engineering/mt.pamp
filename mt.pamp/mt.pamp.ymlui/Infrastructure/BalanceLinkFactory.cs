﻿
using System;
using System.Threading.Tasks;
using mt.pamp.peripherals.BalanceLink;
using mt.pamp.ymlui.State;
using MT.Singularity.Platform.Infrastructure;
using log4net;
using MT.Singularity.Logging;
using mt.pamp.ymlui.Configuration;
using MT.Singularity.Platform.Devices;
using MT.Singularity.IO;
using MT.Singularity.Serialization;
using System.Linq;

namespace mt.pamp.ymlui.Infrastructure
{
    public class BalanceLinkFactory : IBalanceLinkFactoryService
    {
        private readonly ConfigurationStore _configurationStore;
        private readonly IPlatformEngine _platformEngine;
        public event Action<BalanceLink> BalanceLinkChanged;
        public event Action<bool> InitializeBalanceLinkEvent;

        private BalanceLink _currentBalanceLink;

        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(BalanceLinkFactory);

        public BalanceLinkFactory(IPlatformEngine platformEngine, ConfigurationStore configurationStore)
        {
            _platformEngine = platformEngine;
            _configurationStore = configurationStore;
            _configurationStore.ConfigurationChanged += _configurationStore_ConfigurationChanged;

            CreateBalanceLink().ContinueWith(HandleException);
        }

        private void _configurationStore_ConfigurationChanged(PampConfiguration configuration)
        {
            if (_currentBalanceLink?.BalanceLinkConfiguration.BalanceLinkPort != configuration.BalanceLinkPort)
            {
                CreateBalanceLink().ContinueWith(HandleException);
            }
        }

        private void HandleException(Task<BalanceLink> obj)
        {
            if (obj.Exception != null)
            {
                Logger.ErrorEx("BalanceLink could not be initialized", SourceClass, obj.Exception);
                InitializeBalanceLinkEvent?.Invoke(false);
                return;
            }

            _currentBalanceLink = obj.Result;
            BalanceLinkChanged?.Invoke(_currentBalanceLink);
            InitializeBalanceLinkEvent?.Invoke(true);
        }

        public async Task<BalanceLink> CreateBalanceLink()
        {
            var interfaceService = await _platformEngine.GetInterfaceServiceAsync();
            var allInterfaces = await interfaceService.GetAllInterfacesAsync();
            var allSerialInterfaces = allInterfaces.OfType<ISerialInterface>().ToList();
            var serialInterface = allSerialInterfaces.FirstOrDefault(serial => serial.LogicalPort == _configurationStore.CurrentConfiguration.BalanceLinkPort);

            IConnectionChannel<DataSegment> serialConnectionChannel = await serialInterface.CreateConnectionChannelAsync();
            var delimiterSerializer = new DelimiterSerializer(serialConnectionChannel, CommonDataSegments.Newline);
            var serializer = new StringSerializer(delimiterSerializer);
            serializer.CodePage = CodePage.CP1250;
            var balanceLinkConfiguration = new SerialBalanceLinkConfiguration();
            balanceLinkConfiguration.StringSerializer = serializer;
            balanceLinkConfiguration.BalanceLinkPort = _configurationStore.CurrentConfiguration.CameraPort;

            BalanceLink balanceLink = new BalanceLink(balanceLinkConfiguration);

            return balanceLink;
        }
    }
}
