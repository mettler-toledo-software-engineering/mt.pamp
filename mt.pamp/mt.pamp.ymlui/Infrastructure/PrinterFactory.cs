﻿using mt.pamp.ymlui.State;
using System;
using System.Linq;
using System.Threading.Tasks;
using MT.Singularity.Platform.Infrastructure;
using log4net;
using mt.pamp.peripherals.Printer;
using mt.pamp.ymlui.Configuration;
using MT.Singularity.IO;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Devices;
using MT.Singularity.Serialization;


namespace mt.pamp.ymlui.Infrastructure
{
    public class PrinterFactory : IPrinterFactoryService
    {
        private readonly ConfigurationStore _configurationStore;
        private readonly IPlatformEngine _platformEngine;
        public event Action<IApr331Printer> PrinterChanged;
        public event Action<bool> InitializePrinterEvent;
        private IApr331Printer _currentPrinter;

        private static readonly ILog Logger = Log4NetManager.ApplicationLogger;
        private static readonly string SourceClass = nameof(PrinterFactory);

        public PrinterFactory(IPlatformEngine platformEngine, ConfigurationStore configurationStore)
        {
            _platformEngine = platformEngine;
            _configurationStore = configurationStore;
            _configurationStore.ConfigurationChanged += ConfigurationStoreOnConfigurationChanged;

            CreateApr331Printer().ContinueWith(HandleException);
        }

        private void ConfigurationStoreOnConfigurationChanged(PampConfiguration configuration)
        {
            if (_currentPrinter?.PrinterConfiguration.PrinterPort != configuration.PrinterPort)
            {
                CreateApr331Printer().ContinueWith(HandleException);
            }
        }

        private void HandleException(Task<IApr331Printer> obj)
        {
            if (obj.Exception != null)
            {
                Logger.ErrorEx("Printer could not be initialized", SourceClass, obj.Exception);
                InitializePrinterEvent?.Invoke(false);
                return;
            }

            _currentPrinter = obj.Result;
            PrinterChanged?.Invoke(_currentPrinter);
            InitializePrinterEvent?.Invoke(true);
        }

        public async Task<IApr331Printer> CreateApr331Printer()
        {
            var interfaceservice = await _platformEngine.GetInterfaceServiceAsync();
            var allinterfaces = await interfaceservice.GetAllInterfacesAsync();
            var allserialinterfaces = allinterfaces.OfType<ISerialInterface>().ToList();
            var serialInterface = allserialinterfaces.FirstOrDefault(serial => serial.LogicalPort == _configurationStore.CurrentConfiguration.PrinterPort);

            IConnectionChannel<DataSegment> serialConnectionChannel = await serialInterface.CreateConnectionChannelAsync();
            var delimiterSerializer = new DelimiterSerializer(serialConnectionChannel, CommonDataSegments.Newline);
            var serializer = new StringSerializer(delimiterSerializer);
            serializer.CodePage = CodePage.CP1250;
            var printerConfiguration = new SerialPrinterConfiguration();
            printerConfiguration.StringSerializer = serializer;
            printerConfiguration.PrinterPort = _configurationStore.CurrentConfiguration.PrinterPort;

            Apr331Printer printer = new Apr331Printer(printerConfiguration);

            return printer;
        }
    }
}
