﻿using mt.pamp.peripherals.Camera;
using System;
using System.Threading.Tasks;

namespace mt.pamp.ymlui.Infrastructure
{
    public interface ICameraFactoryService
    {
        event Action<Camera> CameraChanged;
        event Action<bool> InitializeCameraEvent;
        Task<Camera> CreateCamera();
    }
}