﻿using System;

namespace mt.pamp.ymlui.Exceptions
{
    public class PrintLabelFailedException : Exception
    {
        public PrintLabelFailedException()
            : base("Failed to print label with APR331")
        {
        }
    }
}
