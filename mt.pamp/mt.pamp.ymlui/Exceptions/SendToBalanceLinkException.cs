﻿using System;

namespace mt.pamp.ymlui.Exceptions
{
    class SendToBalanceLinkException : Exception
    {
        public SendToBalanceLinkException()
            : base("Failed to send weight to BalanceLink")
        {
        }
    }
}
