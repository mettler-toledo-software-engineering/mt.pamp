﻿using System;

namespace mt.pamp.ymlui.Exceptions
{
    public class SendToCameraFailedException : Exception
    {
        public SendToCameraFailedException()
            : base("Failed to send weight string to camera")
        {
        }
    }
}
