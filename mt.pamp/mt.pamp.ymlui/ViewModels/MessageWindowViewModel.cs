﻿using mt.pamp.application.Models;
using mt.pamp.ymlui.Converter;
using mt.pamp.ymlui.State;
using MT.Singularity.Presentation.Drawing;

namespace mt.pamp.ymlui.ViewModels
{
    public class MessageWindowViewModel : ViewModelBase
    {
        private readonly MessageStore _messageStore;
        
        public MessageWindowViewModel(MessageStore messageStore)
        {
            _messageStore = messageStore;
            _messageStore.NewTextMessage += _messageStore_NewTextMessage;

            MessageText = Message.DefaultMessage().MessageText.ConvertToString();
            BackGround = Message.DefaultMessage().MessageColor.ConvertToSolidColorBrush();
        }

        private void _messageStore_NewTextMessage(Message message)
        {
            MessageText = message.MessageText.ConvertToString();
            BackGround = message.MessageColor.ConvertToSolidColorBrush();
        }

        #region Properties

        private string _message;

        public string MessageText
        {
            get { return _message; }
            private set
            {
                _message = value;
                NotifyPropertyChanged(nameof(MessageText));
            }
        }

        private SolidColorBrush _backGround;

        public SolidColorBrush BackGround
        {
            get { return _backGround; }
            private set
            {
                _backGround = value;
                NotifyPropertyChanged(nameof(BackGround));
            }
        }

        #endregion

        public override void Dispose()
        {
            _messageStore.NewTextMessage -= _messageStore_NewTextMessage;
        }
    }
}
