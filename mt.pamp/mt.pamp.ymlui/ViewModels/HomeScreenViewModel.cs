﻿// ReSharper disable once RedundantUsingDirective

using mt.pamp.application.Models;
using mt.pamp.ymlui.Commands;
using mt.pamp.ymlui.Infrastructure;
using mt.pamp.ymlui.State;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Model;

namespace mt.pamp.ymlui.ViewModels
{
    /// <summary>
    /// Home Screen view model implementation
    /// </summary>
    public class HomeScreenViewModel : ViewModelBase
    {
        
        private readonly SumStore _sumStore;
        /// <summary>
        /// Eine Einzelwägung ausführen
        /// </summary>
        public ICommand SingleWeighingCommand { get; }
        /// <summary>
        /// Zum Fenster wechseln, um die einzelnen Wägungen zu summieren
        /// </summary>
        public ICommand GoToSumViewCommand { get; }

        public ICommand ReprintCommand { get; }

        private MessageStore _messageStore;

        /// <summary>
        /// Constructor Initializes a new instance of the <see cref="HomeScreenViewModel"/> class.
        /// </summary>
        public HomeScreenViewModel(MessageStore messageStore, SumStore sumStore, PrinterStore printerStore, BalanceLinkStore balanceLinkStore)
        {
            _sumStore = sumStore;
            _sumStore.CompleteStateChanged += SumStoreOnCompleteStateChanged;
            GoToSumViewCommand = new GoToSumViewCommand(this);
            SingleWeighingCommand = new SingleWeighingCommand(this, messageStore, sumStore, printerStore, balanceLinkStore);
            ReprintCommand = new ReprintCommand(messageStore, sumStore, printerStore);
            _messageStore = messageStore;
            ReprintButtonVisibility = _sumStore.CurrentMeasurement?.Weighings?.Count > 0 ? Visibility.Visible : Visibility.Collapsed;
        }

        private void SumStoreOnCompleteStateChanged()
        {
            ReprintButtonVisibility = _sumStore.CurrentMeasurement?.Weighings?.Count > 0 ? Visibility.Visible : Visibility.Collapsed;
        }

        public override void Dispose()
        {
            
        }

        private Visibility _reprintButtonVisibility;

        public Visibility ReprintButtonVisibility
        {
            get { return _reprintButtonVisibility; }
            set
            {
                _reprintButtonVisibility = value;
                NotifyPropertyChanged(nameof(ReprintButtonVisibility));
            }
        }

        public string Version => $"{Globals.ProgVersionStr(false)}";
        public string ProjectNumber => $"{Globals.ProjectNumber}";

        public override void UpdateProperties()
        {
            _messageStore.UpdateMessage(new Message(Messages.Empty, MessageColors.LightBlue, false));
            base.UpdateProperties();
        }
    }
}
