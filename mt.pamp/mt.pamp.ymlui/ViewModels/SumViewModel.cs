﻿using mt.pamp.application.Models;
using mt.pamp.application.Services.HouseKeeping;
using mt.pamp.ymlui.Commands;
using mt.pamp.ymlui.State;
using MT.Singularity.Presentation.Model;

namespace mt.pamp.ymlui.ViewModels
{
    public class SumViewModel : ViewModelBase
    {
        /// <summary>
        /// Aktuelles Gewicht zum Totalgewicht dazurechnen und String an Kamera senden
        /// </summary>
        public ICommand SumCommand { get; }
        /// <summary>
        /// Zurück zum Hauptfenster kehren, wenn keine Summierung gestartet wurde
        /// </summary>
        public ICommand GoBackCommand { get; }
        /// <summary>
        /// Summierung beenden und String an Kamera senden
        /// </summary>
        public ICommand FinishCommand { get; }

        private MessageStore _messageStore;
        private SumStore _sumStore;
        
        public SumViewModel(MessageStore messageStore, PrinterStore printerStore, SumStore sumStore, CameraStore cameraStore, BalanceLinkStore balanceLinkStore)
        {
            GoBackCommand = new GoBackCommand(this, sumStore, balanceLinkStore);
            SumCommand = new SumCommand(this, messageStore, sumStore, cameraStore, balanceLinkStore);
            FinishCommand = new FinishCommand(this, messageStore, printerStore, sumStore, cameraStore, balanceLinkStore);

            _messageStore = messageStore;
            _sumStore = sumStore;
            _sumStore.NewCurrentWeight += _sumStore_NewCurrentWeight;
            _sumStore.UpdateMeasurement += _sumStore_UpdateMeasurement;
            _sumStore.ResetValues();
        }

        private void _sumStore_NewCurrentWeight()
        {
            _sumStore_UpdateMeasurement(_sumStore.CurrentMeasurement);
        }

        private void _sumStore_UpdateMeasurement(Measurement m)
        {
            TotalWeight = m.NetWeight().ToFormattedWeight();
            SumCounter = $"{m.Weighings.Count}";

            EnableEditOrderInfos(m.Weighings.Count == 0);
        }

        private void EnableEditOrderInfos(bool enable)
        {
            BarcodeIsEnabled = enable;
            CustomerNumberIsEnabled = enable;
        }

        #region Properties
        
        #region Barcode

        private string _barcode;

        public string Barcode
        {
            get { return _barcode; }
            private set
            {
                _barcode = value;
                NotifyPropertyChanged(nameof(Barcode));
                _sumStore.Barcode = Barcode;
            }
        }

        #endregion

        #region _barcodeIsEnabled

        private bool _barcodeIsEnabled;

        public bool BarcodeIsEnabled
        {
            get { return _barcodeIsEnabled; }
            set
            {
                _barcodeIsEnabled = value;
                NotifyPropertyChanged(nameof(BarcodeIsEnabled));
            }
        }

        #endregion

        #region CustomerNumber

        private string _customerNumber;

        public string CustomerNumber
        {
            get { return _customerNumber; }
            private set
            {
                _customerNumber = value;
                NotifyPropertyChanged(nameof(CustomerNumber));
                _sumStore.CustomerNumber = CustomerNumber;
            }
        }

        #endregion

        #region CustomerNumberIsEnabled

        private bool _customerNumberIsEnabled;

        public bool CustomerNumberIsEnabled
        {
            get { return _customerNumberIsEnabled; }
            set
            {
                _customerNumberIsEnabled = value;
                NotifyPropertyChanged(nameof(CustomerNumberIsEnabled));
            }
        }

        #endregion

        #region SumCounter

        private string _sumCounter;

        public string SumCounter
        {
            get { return _sumCounter; }
            private set
            {
                _sumCounter = value;
                NotifyPropertyChanged(nameof(SumCounter));
            }
        }

        #endregion

        #region TotalWeight

        private string _totalWeight;

        public string TotalWeight
        {
            get { return _totalWeight; }
            private set
            {
                _totalWeight = value;
                NotifyPropertyChanged(nameof(TotalWeight));
            }
        }

        #endregion

        #endregion

        public override void UpdateProperties()
        {
            _messageStore.UpdateMessage(new Message(Messages.Empty, MessageColors.LightBlue, false));
            base.UpdateProperties();
        }

        public override void Dispose()
        {
            _sumStore.UpdateMeasurement -= _sumStore_UpdateMeasurement;
        }
    }
}
