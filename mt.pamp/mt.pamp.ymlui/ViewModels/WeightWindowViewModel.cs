﻿using mt.pamp.application.Models;
using mt.pamp.ymlui.Converter;
using mt.pamp.ymlui.State;
using MT.Singularity.Platform.Devices.Scale;

namespace mt.pamp.ymlui.ViewModels
{
    public class WeightWindowViewModel : ViewModelBase
    {
        private readonly MessageStore _messageStore;
        
        public WeightWindowViewModel(MessageStore messageStore, IScaleService scaleService) : base(scaleService)
        {
            //BackGround = MessageColors.LightBlue.ConvertToSolidColorBrush();
            _messageStore = messageStore;
            
            _messageStore.NewWeightMessage += MessageStoreOnNewMessage;
        }

        private void MessageStoreOnNewMessage(Message message)
        {
            BackGround = message.MessageColor.ConvertToSolidColorBrush();
        }

        public override void Dispose()
        {
            _messageStore.NewWeightMessage -= MessageStoreOnNewMessage;
        }
    }
}
