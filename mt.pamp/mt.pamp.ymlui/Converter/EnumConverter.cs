﻿using System;
using mt.pamp.application.Models;
using mt.pamp.ymlui.Infrastructure;
using MT.Singularity.Presentation.Drawing;

namespace mt.pamp.ymlui.Converter
{
    public static class EnumConverter
    {
        public static SolidColorBrush ConvertToSolidColorBrush(this MessageColors messageColors)
        {
            switch (messageColors)
            {
                case MessageColors.Red:
                    return Globals.RedBrush;
                case MessageColors.Yellow:
                    return Globals.YellowBrush;
                case MessageColors.Green:
                    return Globals.GreenBrush;
                case MessageColors.LightBlue:
                    return Globals.LightBlueBrush;
                default:
                    throw new ArgumentOutOfRangeException(nameof(messageColors), messageColors, null);
            }
        }

        public static string ConvertToString(this Messages messages)
        {
            switch (messages)
            {
                case Messages.Empty:
                    return "";
                case Messages.NoWeight:
                    return Localization.Get(Localization.Key.PleaseApplyWeight);
                case Messages.RemoveWeight:
                    return Localization.Get(Localization.Key.WeightAdded);
                case Messages.SendWeightToCamera:
                    return Localization.Get(Localization.Key.SendWeightToCamera);
                case Messages.PrintLabel:
                    return Localization.Get(Localization.Key.PrintLabel);                   
                case Messages.ProcessFinished:
                    return Localization.Get(Localization.Key.ProcessFinished);
                case Messages.ErrorWhileSendingToCamera:
                    return $"{Localization.Get(Localization.Key.ErrorWhileSendingToCamera)}\n\r{Localization.Get(Localization.Key.CheckSettings)}";
                case Messages.ErrorWhilePrintingLabel:
                    return $"{Localization.Get(Localization.Key.ErrorWhilePrintingLabel)}\n\r{Localization.Get(Localization.Key.CheckSettings)}";
                case Messages.ErrorWhileSendingToBalanceLink:
                    return $"{Localization.Get(Localization.Key.ErrorWhileSendingToBalanceLink)}\n\r{Localization.Get(Localization.Key.CheckSettings)}";
                case Messages.InvalidOrderInformation:
                    return $"{Localization.Get(Localization.Key.InvalidOrderInformation)}";

                default:
                    throw new ArgumentOutOfRangeException(nameof(messages), messages, null);
            }
        }
    }
}
