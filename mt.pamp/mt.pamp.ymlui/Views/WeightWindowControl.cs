﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using mt.pamp.ymlui.ViewModels;
using MT.Singularity;
using MT.Singularity.Composition;
using MT.Singularity.Presentation;
using MT.Singularity.Presentation.Controls;

namespace mt.pamp.ymlui.Views
{
    /// <summary>
    /// Interaction logic for WeightWindowControl
    /// </summary>
    [InjectionBehavior(IsSingleton = true)]
    public partial class WeightWindowControl
    {
        private readonly WeightWindowViewModel _viewModel;
        public WeightWindowControl(WeightWindowViewModel viewModel)
        {
            _viewModel = viewModel;
            InitializeComponents();
            myWeightWindow.Activate();
            
        }

        
    }
}
