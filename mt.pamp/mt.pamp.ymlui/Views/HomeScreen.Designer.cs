﻿using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Grid;
using MT.Singularity.Presentation.Controls.DataGrid;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Platform.CommonUX.Controls;
using MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View;
using MT.Singularity.Platform.CommonUX.Controls.DeltaTrac;
using mt.pamp.ymlui.Infrastructure;
namespace mt.pamp.ymlui.Views
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class HomeScreen : MT.Singularity.Presentation.Controls.Navigation.NavigationPage
    {
        public void Update_singleWeighingButtonWidth()
        {
            _singleWeighingButton.Width = Globals.ButtonWidth;
        }
        public void Update_singleWeighingButtonBorderBrush()
        {
            _singleWeighingButton.BorderBrush = Globals.LightGray;
        }
        public void Update_singleWeighingButtonBackground()
        {
            _singleWeighingButton.Background = Globals.LightGray;
        }
        public void Update_reprintButtonWidth()
        {
            _reprintButton.Width = Globals.ButtonWidth;
        }
        public void Update_reprintButtonBorderBrush()
        {
            _reprintButton.BorderBrush = Globals.LightGray;
        }
        public void Update_reprintButtonBackground()
        {
            _reprintButton.Background = Globals.LightGray;
        }
        public void Update_goToSumViewButtonWidth()
        {
            _goToSumViewButton.Width = Globals.ButtonWidth;
        }
        public void Update_goToSumViewButtonBorderBrush()
        {
            _goToSumViewButton.BorderBrush = Globals.LightGray;
        }
        public void Update_goToSumViewButtonBackground()
        {
            _goToSumViewButton.Background = Globals.LightGray;
        }
        private MT.Singularity.Presentation.Controls.DynamicDockPanel _weightWindowControl;
        private MT.Singularity.Presentation.Controls.Grid.DynamicGrid _messageWindowControl;
        private MT.Singularity.Presentation.Controls.DynamicStackPanel _weightButtonsControl;
        private MT.Singularity.Presentation.Controls.Button _singleWeighingButton;
        private MT.Singularity.Presentation.Controls.Button _reprintButton;
        private MT.Singularity.Presentation.Controls.Button _goToSumViewButton;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.DockPanel internal2;
            MT.Singularity.Presentation.Controls.StackPanel internal3;
            MT.Singularity.Presentation.Controls.DockPanel internal4;
            MT.Singularity.Presentation.Controls.GroupPanel internal5;
            MT.Singularity.Presentation.Controls.StackPanel internal6;
            MT.Singularity.Presentation.Controls.StackPanel internal7;
            MT.Singularity.Presentation.Controls.StackPanel internal8;
            MT.Singularity.Presentation.Controls.StackPanel internal9;
            MT.Singularity.Presentation.Controls.Image internal10;
            MT.Singularity.Presentation.Controls.StackPanel internal11;
            MT.Singularity.Presentation.Controls.StackPanel internal12;
            MT.Singularity.Presentation.Controls.TextBlock internal13;
            MT.Singularity.Presentation.Controls.TextBlock internal14;
            MT.Singularity.Presentation.Controls.StackPanel internal15;
            MT.Singularity.Presentation.Controls.TextBlock internal16;
            MT.Singularity.Presentation.Controls.TextBlock internal17;
            MT.Singularity.Presentation.Controls.StackPanel internal18;
            MT.Singularity.Presentation.Controls.Image internal19;
            MT.Singularity.Presentation.Controls.DockPanel internal20;
            MT.Singularity.Presentation.Controls.GroupPanel internal21;
            MT.Singularity.Presentation.Controls.StackPanel internal22;
            MT.Singularity.Presentation.Controls.GroupPanel internal23;
            MT.Singularity.Presentation.Controls.Image internal24;
            MT.Singularity.Presentation.Controls.TextBlock internal25;
            MT.Singularity.Presentation.Controls.GroupPanel internal26;
            MT.Singularity.Presentation.Controls.Image internal27;
            MT.Singularity.Presentation.Controls.TextBlock internal28;
            MT.Singularity.Presentation.Controls.StackPanel internal29;
            MT.Singularity.Presentation.Controls.GroupPanel internal30;
            MT.Singularity.Presentation.Controls.Image internal31;
            MT.Singularity.Presentation.Controls.TextBlock internal32;
            _weightWindowControl = new MT.Singularity.Presentation.Controls.DynamicDockPanel();
            _weightWindowControl.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            _weightWindowControl.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            _weightWindowControl.Height = 200;
            internal3 = new MT.Singularity.Presentation.Controls.StackPanel(_weightWindowControl);
            internal3.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal2 = new MT.Singularity.Presentation.Controls.DockPanel(internal3);
            internal2.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            _messageWindowControl = new MT.Singularity.Presentation.Controls.Grid.DynamicGrid();
            _messageWindowControl.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            _messageWindowControl.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            _messageWindowControl.Height = 100;
            internal7 = new MT.Singularity.Presentation.Controls.StackPanel(_messageWindowControl);
            internal10 = new MT.Singularity.Presentation.Controls.Image();
            internal10.Margin = new MT.Singularity.Presentation.Thickness(20);
            internal10.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal10.Source = Globals.MTLogo;
            internal9 = new MT.Singularity.Presentation.Controls.StackPanel(internal10);
            internal9.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal9.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal13 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal13.FontSize = ((System.Nullable<System.Int32>)34);
            internal13.Text = "Seriennummer:";
            internal14 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal14.Margin = new MT.Singularity.Presentation.Thickness(0, 10, 0, 0);
            internal14.FontSize = ((System.Nullable<System.Int32>)34);
            internal14.Text = "Version:";
            internal12 = new MT.Singularity.Presentation.Controls.StackPanel(internal13, internal14);
            internal12.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal12.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            internal16 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal16.FontSize = ((System.Nullable<System.Int32>)34);
            internal16.Text = "A20071601";
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal16.Text,() => _homeScreenViewModel.ProjectNumber,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal17 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal17.Margin = new MT.Singularity.Presentation.Thickness(0, 10, 0, 0);
            internal17.FontSize = ((System.Nullable<System.Int32>)34);
            internal17.Text = "1.0.0";
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal17.Text,() => _homeScreenViewModel.Version,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal15 = new MT.Singularity.Presentation.Controls.StackPanel(internal16, internal17);
            internal15.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal15.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal15.Margin = new MT.Singularity.Presentation.Thickness(100, 0, 0, 0);
            internal11 = new MT.Singularity.Presentation.Controls.StackPanel(internal12, internal15);
            internal11.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal11.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal11.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal19 = new MT.Singularity.Presentation.Controls.Image();
            internal19.Margin = new MT.Singularity.Presentation.Thickness(20);
            internal19.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal19.Source = Globals.Logo;
            internal18 = new MT.Singularity.Presentation.Controls.StackPanel(internal19);
            internal18.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal18.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal8 = new MT.Singularity.Presentation.Controls.StackPanel(internal9, internal11, internal18);
            internal8.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal8.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal6 = new MT.Singularity.Presentation.Controls.StackPanel(internal7, internal8);
            internal6.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal5 = new MT.Singularity.Presentation.Controls.GroupPanel(internal6);
            internal5.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4294507768u));
            internal4 = new MT.Singularity.Presentation.Controls.DockPanel(internal5);
            internal4.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            _weightButtonsControl = new MT.Singularity.Presentation.Controls.DynamicStackPanel();
            _weightButtonsControl.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            _singleWeighingButton = new MT.Singularity.Presentation.Controls.Button();
            _singleWeighingButton.Margin = new MT.Singularity.Presentation.Thickness(5);
            this.Update_singleWeighingButtonWidth();
            _singleWeighingButton.Height = 90;
            _singleWeighingButton.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.Update_singleWeighingButtonBorderBrush();
            _singleWeighingButton.BorderThickness = 5;
            this.Update_singleWeighingButtonBackground();
            _singleWeighingButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            _singleWeighingButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal24 = new MT.Singularity.Presentation.Controls.Image();
            internal24.Source = Globals.SingleWeighingImage;
            internal24.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal24.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal24.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal25 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal25.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal25.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal25.Text = "Single Weighing";
            internal25.Text = mt.pamp.ymlui.Localization.Get(mt.pamp.ymlui.Localization.Key.SignleWeighing);
            internal25.AddTranslationAction(() => {
                internal25.Text = mt.pamp.ymlui.Localization.Get(mt.pamp.ymlui.Localization.Key.SignleWeighing);
            });
            internal25.FontSize = Globals.ButtonFontSize;
            internal25.Margin = new MT.Singularity.Presentation.Thickness(0, 2, 0, 0);
            internal23 = new MT.Singularity.Presentation.Controls.GroupPanel(internal24, internal25);
            _singleWeighingButton.Content = internal23;
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _singleWeighingButton.Command,() => _homeScreenViewModel.SingleWeighingCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            _reprintButton = new MT.Singularity.Presentation.Controls.Button();
            _reprintButton.Margin = new MT.Singularity.Presentation.Thickness(5);
            this.Update_reprintButtonWidth();
            _reprintButton.Height = 90;
            _reprintButton.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.Update_reprintButtonBorderBrush();
            _reprintButton.BorderThickness = 5;
            this.Update_reprintButtonBackground();
            _reprintButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            _reprintButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _reprintButton.Visibility,() =>  _homeScreenViewModel.ReprintButtonVisibility,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal27 = new MT.Singularity.Presentation.Controls.Image();
            internal27.Source = Globals.PrintImage;
            internal27.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal27.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal27.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal28 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal28.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal28.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal28.Text = "Reprint";
            internal28.Text = mt.pamp.ymlui.Localization.Get(mt.pamp.ymlui.Localization.Key.Reprint);
            internal28.AddTranslationAction(() => {
                internal28.Text = mt.pamp.ymlui.Localization.Get(mt.pamp.ymlui.Localization.Key.Reprint);
            });
            internal28.FontSize = Globals.ButtonFontSize;
            internal28.Margin = new MT.Singularity.Presentation.Thickness(0, 2, 0, 0);
            internal26 = new MT.Singularity.Presentation.Controls.GroupPanel(internal27, internal28);
            _reprintButton.Content = internal26;
            this.bindings[4] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _reprintButton.Command,() => _homeScreenViewModel.ReprintCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal22 = new MT.Singularity.Presentation.Controls.StackPanel(_singleWeighingButton, _reprintButton);
            internal22.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal22.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal22.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal22.Margin = new MT.Singularity.Presentation.Thickness(100, 0, 0, 0);
            _goToSumViewButton = new MT.Singularity.Presentation.Controls.Button();
            _goToSumViewButton.Margin = new MT.Singularity.Presentation.Thickness(5);
            this.Update_goToSumViewButtonWidth();
            _goToSumViewButton.Height = 90;
            _goToSumViewButton.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.Update_goToSumViewButtonBorderBrush();
            _goToSumViewButton.BorderThickness = 5;
            this.Update_goToSumViewButtonBackground();
            _goToSumViewButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            _goToSumViewButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal31 = new MT.Singularity.Presentation.Controls.Image();
            internal31.Source = Globals.StartImage;
            internal31.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal31.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal31.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal32 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal32.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal32.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal32.Text = "Starten";
            internal32.Text = mt.pamp.ymlui.Localization.Get(mt.pamp.ymlui.Localization.Key.Start);
            internal32.AddTranslationAction(() => {
                internal32.Text = mt.pamp.ymlui.Localization.Get(mt.pamp.ymlui.Localization.Key.Start);
            });
            internal32.FontSize = Globals.ButtonFontSize;
            internal32.Margin = new MT.Singularity.Presentation.Thickness(0, 2, 0, 0);
            internal30 = new MT.Singularity.Presentation.Controls.GroupPanel(internal31, internal32);
            _goToSumViewButton.Content = internal30;
            this.bindings[5] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _goToSumViewButton.Command,() => _homeScreenViewModel.GoToSumViewCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal29 = new MT.Singularity.Presentation.Controls.StackPanel(_goToSumViewButton);
            internal29.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal29.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal29.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal21 = new MT.Singularity.Presentation.Controls.GroupPanel(_weightButtonsControl, internal22, internal29);
            internal20 = new MT.Singularity.Presentation.Controls.DockPanel(internal21);
            internal20.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal20.Height = 100;
            internal20.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal20.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4278204523u));
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2, internal4, internal20);
            internal1.PointerDown += OnPointerDown;
            this.Content = internal1;
            this.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            this.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4294507768u));
            this.bindings[6] = MT.Singularity.Expressions.ExpressionBinding.Create(() => this.Background,() =>  _homeScreenViewModel.BackGround,MT.Singularity.Expressions.BindingMode.OneWay,false);
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[7];
    }
}
