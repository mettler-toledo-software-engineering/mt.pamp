﻿using mt.pamp.ymlui.ViewModels;
using MT.Singularity.Presentation.Controls.Navigation;

namespace mt.pamp.ymlui.Views
{
    /// <summary>
    /// Interaction logic for SumView
    /// </summary>
    public partial class SumView
    {
        private readonly SumViewModel _viewModel;
        private readonly WeightWindowControl _weightWindow;
        private readonly MessageWindowControl _messageWindow;

        public SumView(SumViewModel viewModel, WeightWindowControl weightWindow, MessageWindowControl messageWindow)
        {
            _viewModel = viewModel;
            _weightWindow = weightWindow;
            _messageWindow = messageWindow;

            InitializeComponents();
        }

        protected override void OnFirstNavigation()
        {
            _weightWindowControl.Add(_weightWindow);
            _messageWindowControl.Add(_messageWindow);
            _viewModel.UpdateProperties();
            _viewModel.ParentPage = this;

            base.OnFirstNavigation();
        }

        protected override NavigationResult OnNavigatingAway(INavigationPage nextPage)
        {
            var result = base.OnNavigatingAway(nextPage);
            _weightWindowControl.Remove(_weightWindow);
            _messageWindowControl.Remove(_messageWindow);
            return result;
        }

        protected override void OnNavigationReturning(INavigationPage previousPage)
        {
            base.OnNavigationReturning(previousPage);
            _viewModel.ParentPage = this;
            _viewModel.UpdateProperties();
            _weightWindowControl.Add(_weightWindow);
            _messageWindowControl.Add(_messageWindow);
        }

        protected override NavigationResult OnNavigatingBack(INavigationPage nextPage)
        {
            _weightWindowControl.Remove(_weightWindow);
            _messageWindowControl.Remove(_messageWindow);
            return base.OnNavigatingBack(nextPage);
        }
    }
}
