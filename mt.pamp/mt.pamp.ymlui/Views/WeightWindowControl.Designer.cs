﻿using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Grid;
using MT.Singularity.Platform.CommonUX.Controls;
using MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View;
using mt.pamp.ymlui.Infrastructure;
namespace mt.pamp.ymlui.Views
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.2.0.0")]
    public partial class WeightWindowControl : MT.Singularity.Presentation.Controls.ContentControl
    {
        private MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View.WeightDisplayWindow myWeightWindow;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.StackPanel internal2;
            myWeightWindow = new MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View.WeightDisplayWindow();
            myWeightWindow.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4294967295u));
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => myWeightWindow.Background,() => _viewModel.BackGround,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal2 = new MT.Singularity.Presentation.Controls.StackPanel(myWeightWindow);
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2);
            this.Content = internal1;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[1];
    }
}
