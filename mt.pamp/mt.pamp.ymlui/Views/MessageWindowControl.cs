﻿using mt.pamp.ymlui.ViewModels;
using MT.Singularity.Composition;

namespace mt.pamp.ymlui.Views
{
    /// <summary>
    /// Interaction logic for MessageWindowControl
    /// </summary>
    [InjectionBehavior(IsSingleton = true)]
    public partial class MessageWindowControl
    {
        private readonly MessageWindowViewModel _viewModel;
        public MessageWindowControl(MessageWindowViewModel viewModel)
        {
            _viewModel = viewModel;
            InitializeComponents();
        }
    }
}
