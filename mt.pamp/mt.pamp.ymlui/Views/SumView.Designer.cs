﻿using MT.Singularity.Presentation.Controls.Grid;
using MT.Singularity.Presentation.Controls.DataGrid;
using MT.Singularity.Presentation.Controls;
using MT.Singularity.Presentation.Controls.Navigation;
using MT.Singularity.Platform.CommonUX.Controls.MeasurementDisplay.View;
using mt.pamp.ymlui.Infrastructure;
namespace mt.pamp.ymlui.Views
{
    [System.CodeDom.Compiler.GeneratedCodeAttribute("YmlCodeGenerator", "2.1.0.0")]
    public partial class SumView : MT.Singularity.Presentation.Controls.Navigation.NavigationPage
    {
        public void Update_backButtonWidth()
        {
            _backButton.Width = Globals.ButtonWidth;
        }
        public void Update_backButtonBorderBrush()
        {
            _backButton.BorderBrush = Globals.LightGray;
        }
        public void Update_backButtonBackground()
        {
            _backButton.Background = Globals.LightGray;
        }
        public void Update_sumButtonWidth()
        {
            _sumButton.Width = Globals.ButtonWidth;
        }
        public void Update_sumButtonBorderBrush()
        {
            _sumButton.BorderBrush = Globals.LightGray;
        }
        public void Update_sumButtonBackground()
        {
            _sumButton.Background = Globals.LightGray;
        }
        public void Update_reprintRecipeButtonWidth()
        {
            _reprintRecipeButton.Width = Globals.ButtonWidth;
        }
        public void Update_reprintRecipeButtonBorderBrush()
        {
            _reprintRecipeButton.BorderBrush = Globals.LightGray;
        }
        public void Update_reprintRecipeButtonBackground()
        {
            _reprintRecipeButton.Background = Globals.LightGray;
        }
        private MT.Singularity.Presentation.Controls.DynamicDockPanel _weightWindowControl;
        private MT.Singularity.Presentation.Controls.Grid.DynamicGrid _messageWindowControl;
        private MT.Singularity.Presentation.Controls.DynamicStackPanel _weightButtonsControl;
        private MT.Singularity.Presentation.Controls.Button _backButton;
        private MT.Singularity.Presentation.Controls.Button _sumButton;
        private MT.Singularity.Presentation.Controls.Button _reprintRecipeButton;
        private void InitializeComponents()
        {
            MT.Singularity.Presentation.Controls.GroupPanel internal1;
            MT.Singularity.Presentation.Controls.DockPanel internal2;
            MT.Singularity.Presentation.Controls.StackPanel internal3;
            MT.Singularity.Presentation.Controls.StackPanel internal4;
            MT.Singularity.Presentation.Controls.GroupPanel internal5;
            MT.Singularity.Presentation.Controls.StackPanel internal6;
            MT.Singularity.Presentation.Controls.StackPanel internal7;
            MT.Singularity.Presentation.Controls.TextBlock internal8;
            MT.Singularity.Presentation.Controls.TextBox internal9;
            MT.Singularity.Presentation.Controls.StackPanel internal10;
            MT.Singularity.Presentation.Controls.TextBlock internal11;
            MT.Singularity.Presentation.Controls.TextBox internal12;
            MT.Singularity.Presentation.Controls.StackPanel internal13;
            MT.Singularity.Presentation.Controls.TextBlock internal14;
            MT.Singularity.Presentation.Controls.TextBlock internal15;
            MT.Singularity.Presentation.Controls.TextBlock internal16;
            MT.Singularity.Presentation.Controls.StackPanel internal17;
            MT.Singularity.Presentation.Controls.StackPanel internal18;
            MT.Singularity.Presentation.Controls.DockPanel internal19;
            MT.Singularity.Presentation.Controls.GroupPanel internal20;
            MT.Singularity.Presentation.Controls.StackPanel internal21;
            MT.Singularity.Presentation.Controls.GroupPanel internal22;
            MT.Singularity.Presentation.Controls.Image internal23;
            MT.Singularity.Presentation.Controls.TextBlock internal24;
            MT.Singularity.Presentation.Controls.StackPanel internal25;
            MT.Singularity.Presentation.Controls.GroupPanel internal26;
            MT.Singularity.Presentation.Controls.Image internal27;
            MT.Singularity.Presentation.Controls.TextBlock internal28;
            MT.Singularity.Presentation.Controls.StackPanel internal29;
            MT.Singularity.Presentation.Controls.GroupPanel internal30;
            MT.Singularity.Presentation.Controls.Image internal31;
            MT.Singularity.Presentation.Controls.TextBlock internal32;
            internal8 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal8.TextAlignment = MT.Singularity.Presentation.Drawing.TextAlignment.MiddleLeft;
            internal8.Width = 220;
            internal8.Margin = new MT.Singularity.Presentation.Thickness(20, 0, 20, 0);
            internal8.FontSize = ((System.Nullable<System.Int32>)26);
            internal8.Text = "Barcode";
            internal8.Text = mt.pamp.ymlui.Localization.Get(mt.pamp.ymlui.Localization.Key.Barcode);
            internal8.AddTranslationAction(() => {
                internal8.Text = mt.pamp.ymlui.Localization.Get(mt.pamp.ymlui.Localization.Key.Barcode);
            });
            internal9 = new MT.Singularity.Presentation.Controls.TextBox();
            internal9.TextAlignment = MT.Singularity.Presentation.Drawing.TextAlignment.MiddleRight;
            internal9.Width = 180;
            internal9.Height = 50;
            this.bindings[0] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal9.IsEnabled,() =>  _viewModel.BarcodeIsEnabled,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal9.Margin = new MT.Singularity.Presentation.Thickness(20, 0, 20, 10);
            internal9.FontSize = ((System.Nullable<System.Int32>)26);
            internal9.Text = "123456";
            this.bindings[1] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal9.Text,() =>  _viewModel.Barcode,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal7 = new MT.Singularity.Presentation.Controls.StackPanel(internal8, internal9);
            internal7.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal11 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal11.TextAlignment = MT.Singularity.Presentation.Drawing.TextAlignment.MiddleLeft;
            internal11.Width = 220;
            internal11.Margin = new MT.Singularity.Presentation.Thickness(20, 0, 20, 0);
            internal11.FontSize = ((System.Nullable<System.Int32>)26);
            internal11.Text = "Customer number";
            internal11.Text = mt.pamp.ymlui.Localization.Get(mt.pamp.ymlui.Localization.Key.CustomerNumber);
            internal11.AddTranslationAction(() => {
                internal11.Text = mt.pamp.ymlui.Localization.Get(mt.pamp.ymlui.Localization.Key.CustomerNumber);
            });
            internal12 = new MT.Singularity.Presentation.Controls.TextBox();
            internal12.TextAlignment = MT.Singularity.Presentation.Drawing.TextAlignment.MiddleRight;
            internal12.Width = 180;
            internal12.Height = 50;
            this.bindings[2] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal12.IsEnabled,() =>  _viewModel.CustomerNumberIsEnabled,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal12.Margin = new MT.Singularity.Presentation.Thickness(20, 0, 20, 10);
            internal12.FontSize = ((System.Nullable<System.Int32>)26);
            internal12.Text = "987546654";
            this.bindings[3] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal12.Text,() =>  _viewModel.CustomerNumber,MT.Singularity.Expressions.BindingMode.TwoWay,false);
            internal10 = new MT.Singularity.Presentation.Controls.StackPanel(internal11, internal12);
            internal10.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal14 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal14.TextAlignment = MT.Singularity.Presentation.Drawing.TextAlignment.MiddleLeft;
            internal14.Width = 200;
            internal14.Margin = new MT.Singularity.Presentation.Thickness(20, 0, 20, 0);
            internal14.FontSize = ((System.Nullable<System.Int32>)26);
            internal14.Text = "Total";
            internal14.Text = mt.pamp.ymlui.Localization.Get(mt.pamp.ymlui.Localization.Key.Total);
            internal14.AddTranslationAction(() => {
                internal14.Text = mt.pamp.ymlui.Localization.Get(mt.pamp.ymlui.Localization.Key.Total);
            });
            internal15 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal15.TextAlignment = MT.Singularity.Presentation.Drawing.TextAlignment.MiddleRight;
            internal15.Width = 200;
            internal15.Margin = new MT.Singularity.Presentation.Thickness(20, 0, 0, 0);
            internal15.FontSize = ((System.Nullable<System.Int32>)26);
            internal15.Text = "1";
            this.bindings[4] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal15.Text,() =>  _viewModel.SumCounter,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal13 = new MT.Singularity.Presentation.Controls.StackPanel(internal14, internal15);
            internal13.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal6 = new MT.Singularity.Presentation.Controls.StackPanel(internal7, internal10, internal13);
            internal6.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal16 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal16.Width = 440;
            internal16.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal16.TextAlignment = MT.Singularity.Presentation.Drawing.TextAlignment.MiddleRight;
            internal16.Margin = new MT.Singularity.Presentation.Thickness(20, 0, 0, 0);
            internal16.FontSize = ((System.Nullable<System.Int32>)75);
            internal16.Text = "885000.00 g";
            this.bindings[5] = MT.Singularity.Expressions.ExpressionBinding.Create(() => internal16.Text,() =>  _viewModel.TotalWeight,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal5 = new MT.Singularity.Presentation.Controls.GroupPanel(internal6, internal16);
            internal5.Width = 480;
            internal5.Height = 280;
            _weightWindowControl = new MT.Singularity.Presentation.Controls.DynamicDockPanel();
            _weightWindowControl.Height = 480;
            _weightWindowControl.Width = 770;
            _weightWindowControl.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            _weightWindowControl.Visibility = MT.Singularity.Presentation.Visibility.Visible;
            _weightWindowControl.IsEnabled = true;
            internal17 = new MT.Singularity.Presentation.Controls.StackPanel(_weightWindowControl);
            internal17.Margin = new MT.Singularity.Presentation.Thickness(10, 0, 0, 0);
            internal4 = new MT.Singularity.Presentation.Controls.StackPanel(internal5, internal17);
            internal4.Height = 400;
            internal4.Margin = new MT.Singularity.Presentation.Thickness(10);
            internal4.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            _messageWindowControl = new MT.Singularity.Presentation.Controls.Grid.DynamicGrid();
            _messageWindowControl.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            _messageWindowControl.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            _messageWindowControl.Height = 100;
            internal18 = new MT.Singularity.Presentation.Controls.StackPanel(_messageWindowControl);
            internal3 = new MT.Singularity.Presentation.Controls.StackPanel(internal4, internal18);
            internal3.Orientation = MT.Singularity.Presentation.Orientation.Vertical;
            internal2 = new MT.Singularity.Presentation.Controls.DockPanel(internal3);
            _weightButtonsControl = new MT.Singularity.Presentation.Controls.DynamicStackPanel();
            _weightButtonsControl.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            _backButton = new MT.Singularity.Presentation.Controls.Button();
            _backButton.Margin = new MT.Singularity.Presentation.Thickness(5);
            this.Update_backButtonWidth();
            _backButton.Height = 90;
            _backButton.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.Update_backButtonBorderBrush();
            _backButton.BorderThickness = 5;
            this.Update_backButtonBackground();
            _backButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            _backButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal23 = new MT.Singularity.Presentation.Controls.Image();
            internal23.Source = Globals.ArrowLeftImage;
            internal23.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal23.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal23.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal24 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal24.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal24.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal24.Text = "Back";
            internal24.Text = mt.pamp.ymlui.Localization.Get(mt.pamp.ymlui.Localization.Key.Back);
            internal24.AddTranslationAction(() => {
                internal24.Text = mt.pamp.ymlui.Localization.Get(mt.pamp.ymlui.Localization.Key.Back);
            });
            internal24.FontSize = Globals.ButtonFontSize;
            internal24.Margin = new MT.Singularity.Presentation.Thickness(0, 2, 0, 0);
            internal22 = new MT.Singularity.Presentation.Controls.GroupPanel(internal23, internal24);
            _backButton.Content = internal22;
            this.bindings[6] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _backButton.Command,() => _viewModel.GoBackCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal21 = new MT.Singularity.Presentation.Controls.StackPanel(_backButton);
            internal21.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal21.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal21.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Left;
            _sumButton = new MT.Singularity.Presentation.Controls.Button();
            _sumButton.Margin = new MT.Singularity.Presentation.Thickness(5);
            this.Update_sumButtonWidth();
            _sumButton.Height = 90;
            _sumButton.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.Update_sumButtonBorderBrush();
            _sumButton.BorderThickness = 5;
            this.Update_sumButtonBackground();
            _sumButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            _sumButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal27 = new MT.Singularity.Presentation.Controls.Image();
            internal27.Source = Globals.AddImage;
            internal27.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal27.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal27.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal28 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal28.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal28.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal28.Text = "Sum";
            internal28.Text = mt.pamp.ymlui.Localization.Get(mt.pamp.ymlui.Localization.Key.Sum);
            internal28.AddTranslationAction(() => {
                internal28.Text = mt.pamp.ymlui.Localization.Get(mt.pamp.ymlui.Localization.Key.Sum);
            });
            internal28.FontSize = Globals.ButtonFontSize;
            internal28.Margin = new MT.Singularity.Presentation.Thickness(0, 2, 0, 0);
            internal26 = new MT.Singularity.Presentation.Controls.GroupPanel(internal27, internal28);
            _sumButton.Content = internal26;
            this.bindings[7] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _sumButton.Command,() => _viewModel.SumCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal25 = new MT.Singularity.Presentation.Controls.StackPanel(_sumButton);
            internal25.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal25.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal25.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            _reprintRecipeButton = new MT.Singularity.Presentation.Controls.Button();
            _reprintRecipeButton.Margin = new MT.Singularity.Presentation.Thickness(5);
            this.Update_reprintRecipeButtonWidth();
            _reprintRecipeButton.Height = 90;
            _reprintRecipeButton.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            this.Update_reprintRecipeButtonBorderBrush();
            _reprintRecipeButton.BorderThickness = 5;
            this.Update_reprintRecipeButtonBackground();
            _reprintRecipeButton.HorizontalContentAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            _reprintRecipeButton.VerticalContentAlignment = MT.Singularity.Presentation.VerticalAlignment.Stretch;
            internal31 = new MT.Singularity.Presentation.Controls.Image();
            internal31.Source = Globals.OkImage;
            internal31.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal31.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Top;
            internal31.Margin = new MT.Singularity.Presentation.Thickness(4);
            internal32 = new MT.Singularity.Presentation.Controls.TextBlock();
            internal32.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal32.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Center;
            internal32.Text = "Finish";
            internal32.Text = mt.pamp.ymlui.Localization.Get(mt.pamp.ymlui.Localization.Key.Finish);
            internal32.AddTranslationAction(() => {
                internal32.Text = mt.pamp.ymlui.Localization.Get(mt.pamp.ymlui.Localization.Key.Finish);
            });
            internal32.FontSize = Globals.ButtonFontSize;
            internal32.Margin = new MT.Singularity.Presentation.Thickness(0, 2, 0, 0);
            internal30 = new MT.Singularity.Presentation.Controls.GroupPanel(internal31, internal32);
            _reprintRecipeButton.Content = internal30;
            this.bindings[8] = MT.Singularity.Expressions.ExpressionBinding.Create(() => _reprintRecipeButton.Command,() => _viewModel.FinishCommand,MT.Singularity.Expressions.BindingMode.OneWay,false);
            internal29 = new MT.Singularity.Presentation.Controls.StackPanel(_reprintRecipeButton);
            internal29.Orientation = MT.Singularity.Presentation.Orientation.Horizontal;
            internal29.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Center;
            internal29.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Right;
            internal20 = new MT.Singularity.Presentation.Controls.GroupPanel(_weightButtonsControl, internal21, internal25, internal29);
            internal19 = new MT.Singularity.Presentation.Controls.DockPanel(internal20);
            internal19.VerticalAlignment = MT.Singularity.Presentation.VerticalAlignment.Bottom;
            internal19.Height = 100;
            internal19.HorizontalAlignment = MT.Singularity.Presentation.HorizontalAlignment.Stretch;
            internal19.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4278204523u));
            internal1 = new MT.Singularity.Presentation.Controls.GroupPanel(internal2, internal19);
            internal1.Background = new MT.Singularity.Presentation.Drawing.SolidColorBrush(new MT.Singularity.Presentation.Color(4294507768u));
            this.Content = internal1;
            this.Width = 1280;
        }
        private readonly MT.Singularity.Expressions.IDataBinding[] bindings = new MT.Singularity.Expressions.IDataBinding[9];
    }
}
