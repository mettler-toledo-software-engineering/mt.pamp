﻿using System.ComponentModel;
using MT.Singularity.Components;
using MT.Singularity.Platform.Configuration;

namespace mt.pamp.ymlui.Configuration
{
    [Component]
    public class PampConfiguration : ComponentConfiguration
    {
        public PampConfiguration()
        {
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue("")]
        public virtual string TestResult
        {
            get { return (string)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(3)]
        public virtual int PrinterPort
        {
            get { return (int)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(4)]
        public virtual int CameraPort
        {
            get { return (int)GetLocal(); }
            set { SetLocal(value); }
        }

        [Setting(WritePermissions = Permissions.UntilSupervisorId)]
        [Component]
        [DefaultValue(5)]
        public virtual int BalanceLinkPort
        {
            get { return (int)GetLocal(); }
            set { SetLocal(value); }
        }
    }
}
