﻿using System;
using System.Threading.Tasks;
using mt.pamp.application.Services.SystemUpdate;
using MT.Singularity.Collections;
using MT.Singularity.Composition;
using MT.Singularity.Logging;
using MT.Singularity.Platform.CommonUX.Setup.Impl;
using MT.Singularity.Platform.CommonUX.Setup.ViewModels;
using MT.Singularity.Platform.Infrastructure;

namespace mt.pamp.ymlui.Configuration
{
    [Export(typeof(CustomerGroupSetupMenuItem))]
    public class PampSetupNode : CustomerGroupSetupMenuItem
    {
        private readonly ISystemUpdateService _systemUpdate;
        private readonly IPlatformEngine _engine;
        public PampSetupNode(SetupMenuContext context, ISystemUpdateService systemUpdate, IPlatformEngine engine) : base(context, 
            new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.PampMainNode))
        {
            _systemUpdate = systemUpdate;
            _engine = engine;
        }
        public override async Task ShowChildrenAsync()
        {
            try
            {
                var pampComponent = _context.CompositionContainer.Resolve<IPampComponents>();
                PampConfiguration pampConfiguration = await pampComponent.GetConfigurationToChangeAsync();
                Children =
                    Indexable.ImmutableValues<SetupMenuItem>(
                        new GeneralSettingsSubNode(_context, pampComponent, pampConfiguration, _systemUpdate, _engine));
            }
            catch (Exception ex)
            {
                Log4NetManager.ApplicationLogger.Error("Error BalancerSetupnode.ShowChildrenAsync", ex);
            }
        }

    }
}
