﻿using MT.Singularity.Composition;
using MT.Singularity.Platform.Configuration;
using MT.Singularity.Platform.UserManagement;

namespace mt.pamp.ymlui.Configuration
{
    [Export(typeof(IPampComponents))]
    [Export(typeof(IConfigurable))]
    [InjectionBehavior(IsSingleton = true)]
    public class PampComponents : ConfigurationStoreConfigurable<PampConfiguration>, IPampComponents
    {

        public PampComponents(IConfigurationStore configurationStore, ISecurityService securityService, CompositionContainer compositionContainer, bool traceChanges = true)
            : base(configurationStore, securityService, compositionContainer, traceChanges)
        {
        }

        public override string ConfigurationSelector
        {
            get { return "PampConfiguration"; }
        }

        public override string FriendlyName
        {
            get { return "Pamp Configuration"; }
        }

    }
}
