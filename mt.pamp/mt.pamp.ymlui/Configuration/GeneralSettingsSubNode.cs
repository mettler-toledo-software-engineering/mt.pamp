﻿using System;
using System.Threading.Tasks;
using mt.pamp.application.Services.SystemUpdate;
using mt.pamp.ymlui.Infrastructure;
using MT.Singularity.Collections;
using MT.Singularity.Logging;
using MT.Singularity.Platform.CommonUX.Setup.Impl;
using MT.Singularity.Platform.CommonUX.Setup.ViewModels;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Presentation;

namespace mt.pamp.ymlui.Configuration
{
    public class GeneralSettingsSubNode : GroupSetupMenuItem
    {
        private readonly PampConfiguration _configuration;
        private readonly ISystemUpdateService _systemUpdate;
        private readonly IPlatformEngine _engine;
        public GeneralSettingsSubNode(SetupMenuContext context, IPampComponents geistlichComponent, PampConfiguration configuration, ISystemUpdateService systemUpdate, IPlatformEngine engine) 
            : base(context, new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.GeneralSettingsSubNode), configuration, geistlichComponent)
        {
            _configuration = configuration;
            _systemUpdate = systemUpdate;
            _engine = engine;
        }

        public override Task ShowChildrenAsync()
        {
            try
            {
                var usbUpdateResultTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.TestResult);
                var usbUpdateResultTarget = new TextBlockSetupMenuItem(_context, usbUpdateResultTitle, _configuration, "TestResult");

                var printerPortTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.PrinterPort);
                var printerPortTarget = new TextSetupMenuItem(_context, printerPortTitle, _configuration, "PrinterPort");

                var cameraPortTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.CameraPort);
                var cameraPortTarget = new TextSetupMenuItem(_context, cameraPortTitle, _configuration, "CameraPort");

                var balanceLinkPortTitle = new TitleAndSubtitle(Localization.GetTranslationModule(), (int)Localization.Key.BalanceLinkPort);
                var balanceLinkPortortTarget = new TextSetupMenuItem(_context, balanceLinkPortTitle, _configuration, "BalanceLinkPort");

                var updateButton = new ImageButtonSetupMenuItem(_context, Globals.FlashDiskImage, new Color(0xFF, 0x00, 0x00, 0x00), Localization.GetTranslationModule(), (int)Localization.Key.Update, 20, SystemUpdate);
                var restartButton = new ImageButtonSetupMenuItem(_context, Globals.RestartImage, new Color(0xFF, 0x00, 0x00, 0x00), Localization.GetTranslationModule(), (int)Localization.Key.Restart, 20, RestartSystem);
                
                var rangeGroup3 = new GroupedSetupMenuItems(_context, printerPortTarget, cameraPortTarget, balanceLinkPortortTarget);
                
                Children = Indexable.ImmutableValues<SetupMenuItem>(rangeGroup3, restartButton, updateButton);
            } 
            catch (Exception ex)
            {
                Log4NetManager.ApplicationLogger.Error("Error BalancerDataServiceSubnode.ShowChildrenAsync", ex);
            }



            return TaskEx.CompletedTask;
        }

        private void RestartSystem()
        {
            _engine.RebootAsync();
        }

        private void SystemUpdate()
        {
            var success = _systemUpdate.ExecuteUpdate();

            if (success == true)
            {
                _configuration.TestResult = "Update copied.";
            }
            else
            {
                _configuration.TestResult = "Update failed!";
            }
        }
    }
}
