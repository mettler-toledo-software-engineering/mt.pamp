﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using mt.pamp.application.Models;
using mt.pamp.ymlui.State;
using mt.pamp.ymlui.ViewModels;

namespace mt.pamp.ymlui.Commands
{
    public class CreateTestDataCommand : AsyncCommandBase
    {
        private readonly MeasurementStore _measurementStore;
        private readonly OrderStore _orderStore;
        public CreateTestDataCommand(OrderStore orderStore, MeasurementStore measurementStore)
        {
            _measurementStore = measurementStore;
            _orderStore = orderStore;
        }
        protected override async Task ExecuteAsync(object parameter)
        {
            await Task.Run(async ()  => 
            {
                for (int i = 0; i < 35000; i++)
                {
                    Random rnd = new Random();

                    await _measurementStore.CreateMeasurement(new Measurement
                    {

                        NetWeight = rnd.Next(115, 149),
                        TimeStamp = DateTime.Now,
                        Order = _orderStore.CurrentOrder

                    });
                }
            });

        }
    }
}
