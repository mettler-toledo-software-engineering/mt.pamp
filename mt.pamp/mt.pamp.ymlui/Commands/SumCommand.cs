﻿using mt.pamp.application.Models;
using mt.pamp.ymlui.Exceptions;
using mt.pamp.ymlui.State;
using mt.pamp.ymlui.ViewModels;
using MT.Singularity.Logging;
using System.Threading;
using System.Threading.Tasks;

namespace mt.pamp.ymlui.Commands
{
    public class SumCommand : AsyncCommandBase
    {
        private readonly SumViewModel _viewModel;
        private readonly MessageStore _messageStore;
        private readonly SumStore _sumStore;
        private readonly CameraStore _cameraStore;
        private readonly BalanceLinkStore _balanceLinkStore;

        private bool _oldWeightRemoved = true;
        private bool _updateMessage = true;

        public SumCommand(SumViewModel viewModel, MessageStore messageStore, SumStore sumStore, CameraStore cameraStore, BalanceLinkStore balanceLinkStore)
        {
            _viewModel = viewModel;
            _messageStore = messageStore;
            _sumStore = sumStore;
            _cameraStore = cameraStore;
            _balanceLinkStore = balanceLinkStore;
            
            _sumStore.NewCurrentWeight += _sumStore_NewCurrentWeight;
            _sumStore.CompleteStateChanged += _sumStore_CompleteStateChanged;
            _cameraStore.CameraChanged += _cameraStore_CameraChanged;
            _balanceLinkStore.BalanceLinkChanged += _balanceLinkStore_BalanceLinkChanged;
        }

        private void _balanceLinkStore_BalanceLinkChanged()
        {
            _sumStore.CompleteMeasurement(_sumStore.IsCompleting, false, false, false);
        }

        private void _cameraStore_CameraChanged()
        {
            _sumStore.CompleteMeasurement(_sumStore.IsCompleting, false, false, false);
        }

        private void _sumStore_CompleteStateChanged()
        {
            OnCanExecuteChanged();
        }

        private void _sumStore_NewCurrentWeight()
        {
            if (!_sumStore.IsCompleting && _updateMessage)
            {
                _messageStore.UpdateMessage(new Message(Messages.Empty, MessageColors.LightBlue, false));
            }

            OnCanExecuteChanged();
        }

        public override bool CanExecute(object parameter)
        {
            if (_sumStore.CurrentWeightInformation.NetStable)
            {
                if (string.IsNullOrEmpty(_sumStore.Barcode) || string.IsNullOrEmpty(_sumStore.CustomerNumber))
                {
                    _messageStore.UpdateMessage(new Message(Messages.InvalidOrderInformation, MessageColors.Yellow, false));
                }
                else if (_sumStore.CurrentWeightInformation.NetWeight == 0)
                {
                    _oldWeightRemoved = true;
                    _messageStore.UpdateMessage(new Message(Messages.NoWeight, MessageColors.Yellow, false));
                }
            }

            return base.CanExecute(parameter) && (_sumStore.CurrentWeightInformation.NetStable &&
                _sumStore.CurrentWeightInformation.NetWeight > 0 && !_sumStore.IsCompleting && _oldWeightRemoved)
                && (!string.IsNullOrEmpty(_sumStore.Barcode) && !string.IsNullOrEmpty(_sumStore.CustomerNumber));
        }        
        
        protected override async Task ExecuteAsync(object parameter)
        {
            try
            {
                CheckInitializations();

                _updateMessage = true;
                _messageStore.UpdateMessage(new Message(Messages.SendWeightToCamera, MessageColors.LightBlue, false));

                await _balanceLinkStore.SendCurrentWeight(_sumStore.CurrentWeighing.NetWeight);

                _sumStore.AddWeightToTotalWeight();
                _oldWeightRemoved = false;

                if (_sumStore.SumCounter == 1)
                {
                    await _cameraStore.SendEmptyString();
                    Thread.Sleep(250);
                }

                await _cameraStore.SendCurrentWeight(_sumStore.Barcode, _sumStore.CustomerNumber, _sumStore.CurrentWeighing);
                Thread.Sleep(2000);

                _messageStore.UpdateMessage(new Message(Messages.RemoveWeight, MessageColors.Green, false));
                Thread.Sleep(500);
            }
            catch (SendToBalanceLinkException e)
            {
                Logger.ErrorEx(e.Message, SourceClass, e);
                _sumStore.CompleteMeasurement(true, false, false);
                _messageStore.UpdateMessage(new Message(Messages.ErrorWhileSendingToBalanceLink, MessageColors.Red, false));
            }
            catch (SendToCameraFailedException e)
            {
                _updateMessage = false;
                Logger.ErrorEx(e.Message, SourceClass, e);
                _messageStore.UpdateMessage(new Message(Messages.ErrorWhileSendingToCamera, MessageColors.Red, false));
            }
        }

        private void CheckInitializations()
        {
            if (!_balanceLinkStore.InitializeBalanceLinkSuccess)
            {
                throw new SendToBalanceLinkException();
            }

            if (!_cameraStore.InitializeCameraSuccess)
            {
                throw new SendToCameraFailedException();
            }
        }
    }
}
