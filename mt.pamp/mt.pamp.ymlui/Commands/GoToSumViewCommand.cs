﻿using mt.pamp.ymlui.ViewModels;
using mt.pamp.ymlui.Views;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Composition;
using System;

namespace mt.pamp.ymlui.Commands
{
    public class GoToSumViewCommand : CommandBase
    {
        private readonly HomeScreenViewModel _viewModel;

        public GoToSumViewCommand(HomeScreenViewModel homeScreenViewModel)
        {
            _viewModel = homeScreenViewModel;
        }

        public override void Execute(object parameter)
        {
            try
            {
                _viewModel.ParentPage.NavigationService.NavigateTo(ApplicationBootstrapperBase.CompositionContainer.Resolve<SumView>());
            }
            catch(Exception e)
            {
                Logger.ErrorEx(e.Message, SourceClass, e);
            }
        }
    }
}
