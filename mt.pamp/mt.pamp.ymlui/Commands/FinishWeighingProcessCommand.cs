﻿using System;
using System.Threading.Tasks;
using mt.pamp.ymlui.State;

namespace mt.pamp.ymlui.Commands
{
    public class FinishWeighingProcessCommand : AsyncCommandBase
    {
        //private readonly WeighingProcessViewModel _weighingProcessViewModel;
        private readonly MessageStore _messageStore;
        private readonly WeighingProcessStore _weighingProcessStore;
        private readonly PrinterStore _printerStore;


        public FinishWeighingProcessCommand(/*WeighingProcessViewModel weighingProcessViewModel,*/ MessageStore messageStore, WeighingProcessStore weighingProcessStore, PrinterStore printerStore)
        {
            //_weighingProcessViewModel = weighingProcessViewModel;
            _weighingProcessStore = weighingProcessStore;
            _messageStore = messageStore;
            _printerStore = printerStore;
        }

        protected override Task ExecuteAsync(object parameter)
        {
            throw new NotImplementedException();
        }

        //protected override async Task ExecuteAsync(object parameter)
        //{
        //    if (_weighingProcessViewModel.MeasurementCount == 0)
        //    {
        //        try
        //        {
        //            _weighingProcessStore.StopweighingProcess();
        //        }
        //        catch (Exception e)
        //        {
        //            Logger.ErrorEx(e.Message, SourceClass, e);
        //        }

        //        _weighingProcessViewModel.ProcessFinishedVisibility = Visibility.Visible;
        //        _weighingProcessViewModel.ProcessOngoingVisibility = Visibility.Collapsed;

        //        return;
        //    }

        //    var questionMb = MessageBox.Show(_weighingProcessViewModel.ParentVisual, "Sind Sie sicher, dass Sie den Prozess beenden wollen?\nAscnliessend können keine weiteren Messungen erfasst werden.", "Prozess beenden?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        //    questionMb.Close();

        //    var result = await questionMb.ShowAsync(_weighingProcessViewModel.ParentVisual);

        //    if (result == DialogResult.Yes)
        //    {
        //        questionMb?.Close();
        //        _messageStore.UpdateMessage(new Message(Messages.PleaseWait, MessageColors.LightBlue, false));                               

        //        _weighingProcessStore.StopweighingProcess();

        //        _weighingProcessViewModel.ProcessFinishedVisibility = Visibility.Visible;
        //        _weighingProcessViewModel.ProcessOngoingVisibility = Visibility.Collapsed;                
        //    }
        //}

    }
}
