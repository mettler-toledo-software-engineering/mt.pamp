﻿using System;
using System.Threading.Tasks;
using mt.pamp.ymlui.State;

namespace mt.pamp.ymlui.Commands
{
    public class LeaveWeighingProcessCommand : AsyncCommandBase
    {
        //private readonly WeighingProcessViewModel _weighingProcessViewModel;
        private readonly SumStore _measurementStore;
        private readonly PrinterStore _printerStore;

        public LeaveWeighingProcessCommand(/*WeighingProcessViewModel weighingProcessViewModel,*/ SumStore measurementStore, PrinterStore printerStore)
        {
            //_weighingProcessViewModel = weighingProcessViewModel;
            _measurementStore = measurementStore;
            _printerStore = printerStore;
        }

        protected override Task ExecuteAsync(object parameter)
        {
            throw new NotImplementedException();
        }

        //protected override async Task ExecuteAsync(object parameter)
        //{
        //    var mb = MessageBox.Show(_weighingProcessViewModel.ParentVisual, "Sind Sie sicher, dass Sie den Prozess verlassen wollen?\nAscnliessend werden alle gesammelten Daten gelöscht.\nEs werden keine weiteren Ausdrucke mehr möglich sein.", "Prozess Verlassen?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
        //    mb.Close();

        //    var result = await mb.ShowAsync(_weighingProcessViewModel.ParentVisual);
        //}
    }
}
