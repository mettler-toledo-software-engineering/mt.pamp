﻿using mt.pamp.ymlui.Exceptions;
using mt.pamp.ymlui.State;
using mt.pamp.ymlui.ViewModels;
using MT.Singularity.Logging;
using MT.Singularity.Presentation.Controls;
using System;
using System.Threading.Tasks;

namespace mt.pamp.ymlui.Commands
{
    public class GoBackCommand : AsyncCommandBase
    {
        private readonly SumViewModel _viewModel;
        private readonly SumStore _sumStore;
        private readonly BalanceLinkStore _balanceLinkStore;
        
        public GoBackCommand(SumViewModel viewModel, SumStore sumStore, BalanceLinkStore balanceLinkStore)
        {
            _viewModel = viewModel;
            _sumStore = sumStore;
            _balanceLinkStore = balanceLinkStore;

            _sumStore.NewCurrentWeight += _sumStore_NewCurrentWeight;
            _sumStore.CompleteStateChanged += _sumStore_CompleteStateChanged;
            _balanceLinkStore.BalanceLinkChanged += _balanceLinkStore_BalanceLinkChanged;
        }

        private void _balanceLinkStore_BalanceLinkChanged()
        {
            _sumStore.CompleteMeasurement(_sumStore.IsCompleting, false, false, false);
        }

        private void _sumStore_CompleteStateChanged()
        {
            OnCanExecuteChanged();
        }

        private void _sumStore_NewCurrentWeight()
        {
            OnCanExecuteChanged();
        }

        public override bool CanExecute(object parameter)
        {
            return base.CanExecute(parameter) && !_sumStore.IsCompleting || 
                (_sumStore.IsCompleting && (!_sumStore.PrintSucceeded || !_sumStore.SendToCameraSucceed));
        }

        protected override async Task ExecuteAsync(object parameter)
        {
            try
            {
                if (_sumStore.CurrentMeasurement.Weighings.Count > 0)
                {
                    await ShowQuestionBox();
                    return;
                }

                GoBackToMainPage();
            }
            catch (Exception e)
            {
                Logger.ErrorEx(e.Message, SourceClass, e);
            }
        }

        private async Task ShowQuestionBox()
        {
            string message = $"{Localization.Get(Localization.Key.DoYouWantToQuitMeasurement)}\n\r{Localization.Get(Localization.Key.WeighingsIrrevocablyGone)}";
            string title = Localization.Get(Localization.Key.Cancel);
            var mb = MessageBox.Show(_viewModel.ParentVisual, message, title, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            mb.Close();
            var result = await mb.ShowAsync(_viewModel.ParentVisual);

            if (result == DialogResult.Yes)
            {
                GoBackToMainPage();
            }
        }

        private async void GoBackToMainPage()
        {
            try
            {
                CheckInitializations();

                await _balanceLinkStore.SendCurrentWeight(_sumStore.TotalWeight);

            }
            catch (SendToBalanceLinkException e)
            {
                Logger.ErrorEx(e.Message, SourceClass, e);
                _sumStore.CompleteMeasurement(true, false, false);
                //_messageStore.UpdateMessage(new Message(Messages.ErrorWhileSendingToBalanceLink, MessageColors.Red, false));
            }
            finally
            {
                _viewModel.ParentPage.NavigationService.Back();
            }
        }

        private void CheckInitializations()
        {
            if (!_balanceLinkStore.InitializeBalanceLinkSuccess)
            {
                throw new SendToBalanceLinkException();
            }
        }
    }
}
