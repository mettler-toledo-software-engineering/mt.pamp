﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;
using log4net;
using mt.pamp.application.Models;
using mt.pamp.application.Services;
using mt.pamp.ymlui.State;
using mt.pamp.ymlui.ViewModels;
using MT.Singularity.Logging;

namespace mt.pamp.ymlui.Commands
{
    public class UpdateRecipeCommand : AsyncCommandBase
    {

        private readonly RecipeStore _recipeStore;
        private readonly ManageRecipesViewModel _manageRecipesViewModel;
        public UpdateRecipeCommand(RecipeStore recipeStore, ManageRecipesViewModel manageRecipesViewModel)
        {
            _recipeStore = recipeStore;
            _manageRecipesViewModel = manageRecipesViewModel;

            _manageRecipesViewModel.PropertyChanged += ViewModelOnPropertyChanged;
        }

        private void ViewModelOnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnCanExecuteChanged();
        }

        public override bool CanExecute(object parameter)
        {
            return base.CanExecute(parameter) && _manageRecipesViewModel.SelectedRecipe != null && _manageRecipesViewModel.SelectedRecipe.IsVaild();
        }

        protected override async Task ExecuteAsync(object parameter)
        {
            try
            {
                var recipe = _manageRecipesViewModel.SelectedRecipe;
                recipe.LastModified = DateTime.Now;
                
                await _recipeStore.UpdateRecipe(recipe);
            }
            catch (Exception e)
            {
                Logger.ErrorEx(e.Message, SourceClass, e);
            }
        }
    }
}
