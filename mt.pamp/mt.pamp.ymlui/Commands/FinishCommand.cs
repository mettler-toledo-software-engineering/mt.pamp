﻿using mt.pamp.application.Models;
using mt.pamp.ymlui.Exceptions;
using mt.pamp.ymlui.State;
using mt.pamp.ymlui.ViewModels;
using MT.Singularity.Logging;
using System.Threading;
using System.Threading.Tasks;

namespace mt.pamp.ymlui.Commands
{
    public class FinishCommand : AsyncCommandBase
    {
        private readonly SumViewModel _viewModel;
        private readonly MessageStore _messageStore;
        private readonly PrinterStore _printerStore;
        private readonly SumStore _sumStore;
        private readonly CameraStore _cameraStore;
        private readonly BalanceLinkStore _balanceLinkStore;

        public FinishCommand(SumViewModel viewModel, MessageStore messageStore, PrinterStore printerStore, SumStore sumStore, CameraStore cameraStore, BalanceLinkStore balanceLinkStore)
        {
            _viewModel = viewModel;
            _messageStore = messageStore;
            _printerStore = printerStore;
            _sumStore = sumStore;
            _cameraStore = cameraStore;
            _balanceLinkStore = balanceLinkStore;
            
            _sumStore.UpdateMeasurement += _sumStore_UpdateMeasurement;
            _sumStore.CompleteStateChanged += _sumStore_CompleteStateChanged;
            _cameraStore.CameraChanged += _cameraStore_CameraChanged;
            _printerStore.PrinterChanged += _printerStore_PrinterChanged;
            _balanceLinkStore.BalanceLinkChanged += _balanceLinkStore_BalanceLinkChanged;
        }

        private void _balanceLinkStore_BalanceLinkChanged()
        {
            _sumStore.CompleteMeasurement(_sumStore.IsCompleting, false, false, false);
        }

        private void _cameraStore_CameraChanged()
        {
            _sumStore.CompleteMeasurement(_sumStore.IsCompleting, false, false, false);
        }

        private void _sumStore_UpdateMeasurement(Measurement obj)
        {
            OnCanExecuteChanged();
        }

        private void _printerStore_PrinterChanged()
        {
            _sumStore.CompleteMeasurement(_sumStore.IsCompleting, false, true, false);
        }

        private void _sumStore_CompleteStateChanged()
        {
            OnCanExecuteChanged();
        }
        
        public override bool CanExecute(object parameter)
        {
            return base.CanExecute(parameter) && (_sumStore.CurrentMeasurement != null &&
                _sumStore.CurrentMeasurement.Weighings.Count > 0 && !_sumStore.IsCompleting) || 
                (_sumStore.IsCompleting && (!_sumStore.PrintSucceeded || !_sumStore.SendToCameraSucceed));
        }

        protected override async Task ExecuteAsync(object parameter)
        {
            try
            {
                CheckInitializations();

                _sumStore.CompleteMeasurement();
                _messageStore.UpdateMessage(new Message(Messages.SendWeightToCamera, MessageColors.LightBlue, false));

                await _balanceLinkStore.SendCurrentWeight(_sumStore.TotalWeight);

                await _cameraStore.SendTotalWeight(_sumStore.Barcode, _sumStore.CustomerNumber, _sumStore.CurrentMeasurement);
                Thread.Sleep(2000);

                _messageStore.UpdateMessage(new Message(Messages.PrintLabel, MessageColors.LightBlue, false));

                await _printerStore.PrintLabel(_sumStore.CurrentMeasurement, _sumStore.Barcode, _sumStore.CustomerNumber);

                _messageStore.UpdateMessage(new Message(Messages.ProcessFinished, MessageColors.Green, false));
                Thread.Sleep(500);

                _sumStore.CompleteMeasurement(true, true, true, true);

                _messageStore.UpdateMessage(new Message(Messages.Empty, MessageColors.LightBlue, false));

                _viewModel.ParentPage.NavigationService.Back();
            }
            catch (SendToBalanceLinkException e)
            {
                Logger.ErrorEx(e.Message, SourceClass, e);
                _sumStore.CompleteMeasurement(true, false, false);
                _messageStore.UpdateMessage(new Message(Messages.ErrorWhileSendingToBalanceLink, MessageColors.Red, false));
            }
            catch (SendToCameraFailedException e)
            {
                Logger.ErrorEx(e.Message, SourceClass, e);
                _sumStore.CompleteMeasurement(true, false, false);
                _messageStore.UpdateMessage(new Message(Messages.ErrorWhileSendingToCamera, MessageColors.Red, false));
            }
            catch (PrintLabelFailedException e)
            {
                Logger.ErrorEx(e.Message, SourceClass, e);
                _sumStore.CompleteMeasurement(true, false, true);
                _messageStore.UpdateMessage(new Message(Messages.ErrorWhilePrintingLabel, MessageColors.Red, false));
            }
        }

        private void CheckInitializations()
        {
            if (!_balanceLinkStore.InitializeBalanceLinkSuccess)
            {
                throw new SendToBalanceLinkException();
            }

            if (!_cameraStore.InitializeCameraSuccess)
            {
                throw new SendToCameraFailedException();
            }

            if (!_printerStore.InitializePrinterSuccess)
            {
                throw new PrintLabelFailedException();
            }
        }
    }
}
