﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using log4net;
using mt.pamp.ymlui.State;
using mt.pamp.ymlui.ViewModels;
using MT.Singularity.Logging;

namespace mt.pamp.ymlui.Commands
{
    public class StartWeighingProcessComnmand : AsyncCommandBase
    {

        private readonly WeighingProcessStore _weighingProcessStore;
        private readonly PrinterStore _printerStore;

        public StartWeighingProcessComnmand(WeighingProcessStore weighingProcessStore, PrinterStore printerStore )
        {
            _weighingProcessStore = weighingProcessStore;
            _printerStore = printerStore;

        }
        protected override async Task ExecuteAsync(object parameter)
        {
            await Task.Run(async () =>
            {
                try
                {
                    //_weighingProcessStore.StartweighingProcess();
                    await _printerStore.PrintHeader();
                }
                catch (Exception e)
                {
                    Logger.ErrorEx(e.Message, SourceClass, e);
                }

            });

        }
    }
}
