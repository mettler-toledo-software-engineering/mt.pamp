﻿using mt.pamp.application.Models;
using mt.pamp.ymlui.Exceptions;
using mt.pamp.ymlui.State;
using mt.pamp.ymlui.ViewModels;
using MT.Singularity.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace mt.pamp.ymlui.Commands
{
    public class SingleWeighingCommand : AsyncCommandBase
    {
        private readonly HomeScreenViewModel _viewModel;
        private readonly MessageStore _messageStore;
        private readonly SumStore _sumStore;
        private readonly PrinterStore _printerStore;
        private readonly BalanceLinkStore _balanceLinkStore;

        private Weighing _currentWeighing;
        
        public SingleWeighingCommand(HomeScreenViewModel viewModel, MessageStore messageStore, SumStore sumStore, PrinterStore printerStore, BalanceLinkStore balanceLinkStore)
        {
            _viewModel = viewModel;
            _messageStore = messageStore;
            _sumStore = sumStore;
            _printerStore = printerStore;
            _balanceLinkStore = balanceLinkStore;

            _sumStore.NewCurrentWeight += _sumStore_NewCurrentWeight;
            _printerStore.PrinterChanged += _printerStore_PrinterChanged;
            _balanceLinkStore.BalanceLinkChanged += _balanceLinkStore_BalanceLinkChanged;
        }

        private void _balanceLinkStore_BalanceLinkChanged()
        {
            _sumStore.CompleteMeasurement(_sumStore.IsCompleting, false, false, false);

            _messageStore.UpdateMessage(new Message(Messages.Empty, MessageColors.LightBlue, false));
        }

        private void _printerStore_PrinterChanged()
        {
            _sumStore.CompleteMeasurement(_sumStore.IsCompleting, false, false, false);

            _messageStore.UpdateMessage(new Message(Messages.Empty, MessageColors.LightBlue, false));
        }

        private void _sumStore_NewCurrentWeight()
        {
            _currentWeighing = _sumStore.CurrentWeighing;
        }

        protected override async Task ExecuteAsync(object parameter)
        {
            try
            {
                CheckInitializations();

                await _balanceLinkStore.SendCurrentWeight(_currentWeighing.NetWeight);

                await _printerStore.PrintLabel(_currentWeighing);
            }
            catch (SendToBalanceLinkException e)
            {
                Logger.ErrorEx(e.Message, SourceClass, e);
                _sumStore.CompleteMeasurement(true, false, false);
                _messageStore.UpdateMessage(new Message(Messages.ErrorWhileSendingToBalanceLink, MessageColors.Red, false));
            }
            catch (PrintLabelFailedException e)
            {
                Logger.ErrorEx(e.Message, SourceClass, e);
                _sumStore.CompleteMeasurement(true, false, true);
                _messageStore.UpdateMessage(new Message(Messages.ErrorWhilePrintingLabel, MessageColors.Red, false));
            }
        }

        private void CheckInitializations()
        {
            if (!_balanceLinkStore.InitializeBalanceLinkSuccess)
            {
                throw new SendToBalanceLinkException();
            }

            if (!_printerStore.InitializePrinterSuccess)
            {
                throw new PrintLabelFailedException();
            }

            if (_currentWeighing == null)
            {
                throw new Exception("Invalid weight");
            }

        }
    }
}
