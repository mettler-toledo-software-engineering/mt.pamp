﻿using System;

using System.Collections.Generic;
using System.Text;
using log4net;
using mt.pamp.ymlui.State;
using mt.pamp.ymlui.ViewModels;
using mt.pamp.ymlui.Views;
using MT.Singularity.Composition;
using MT.Singularity.Logging;
using MT.Singularity.Platform.Infrastructure;
using MT.Singularity.Presentation.Controls;


namespace mt.pamp.ymlui.Commands
{
    public class ResumeUnfinishedOrderCommand : CommandBase
    {

        private readonly HomeScreenViewModel _homeScreenViewModel;
        private readonly OrderStore _orderStore;

        public ResumeUnfinishedOrderCommand(HomeScreenViewModel homeScreenViewModel, OrderStore orderStore)
        {
            _homeScreenViewModel = homeScreenViewModel;
            _orderStore = orderStore;

        }
        public override void Execute(object parameter)
        {
            if (_orderStore.CurrentOrder != null)
            {
                MessageBox.Show(_homeScreenViewModel.ParentVisual, $"Offener Auftrag wird fortgesetzt", "Achtung", MessageBoxButtons.OK, MessageBoxIcon.Information, result =>
                {
                    if (result == DialogResult.OK)
                    {
                        try
                        {
                            _homeScreenViewModel.ParentPage.NavigationService.NavigateTo(ApplicationBootstrapperBase.CompositionContainer.Resolve<WeighingProcessView>());
                        }
                        catch (Exception e)
                        {
                            Logger.ErrorEx(e.Message, SourceClass, e);
                        }
                    }
                });
            }
            
        }
    }
}
