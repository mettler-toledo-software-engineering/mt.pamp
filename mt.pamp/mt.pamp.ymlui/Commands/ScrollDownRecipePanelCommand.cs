﻿using System;

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using log4net;
using mt.pamp.application.Models;
using mt.pamp.ymlui.ViewModels;
using MT.Singularity.Logging;

namespace mt.pamp.ymlui.Commands
{
    public class ScrollDownRecipePanelCommand : CommandBase
    {

        private readonly PickRecipeViewModel _pickRecipeViewModel;
        public ScrollDownRecipePanelCommand(PickRecipeViewModel pickRecipeViewModel)
        {
            _pickRecipeViewModel = pickRecipeViewModel;
        }
        public override void Execute(object parameter)
        {
            try
            {
                if (_pickRecipeViewModel.PanelIndex + _pickRecipeViewModel.NumberOfElements >= _pickRecipeViewModel.AllRecipes.Count)
                {
                    return;
                }
                else
                {
                    _pickRecipeViewModel.PanelIndex = _pickRecipeViewModel.PanelIndex + _pickRecipeViewModel.NumberOfElements;
                }
                _pickRecipeViewModel.Recipes = new ObservableCollection<Recipe>(_pickRecipeViewModel.AllRecipes.ToList().GetRange(_pickRecipeViewModel.PanelIndex, _pickRecipeViewModel.NumberOfElements));
            }
            catch (Exception e)
            {
                Logger.ErrorEx(e.Message, SourceClass, e);
            }

          
        }
    }
}
