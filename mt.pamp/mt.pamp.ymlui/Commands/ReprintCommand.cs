﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using mt.pamp.application.Models;
using mt.pamp.ymlui.Exceptions;
using mt.pamp.ymlui.State;
using MT.Singularity.Logging;

namespace mt.pamp.ymlui.Commands
{
    public class ReprintCommand : AsyncCommandBase
    {
        private readonly MessageStore _messageStore;
        private readonly PrinterStore _printerStore;
        private readonly SumStore _sumStore;

        public ReprintCommand(MessageStore messageStore, SumStore sumStore, PrinterStore printerStore)
        {
            _messageStore = messageStore;
            _sumStore = sumStore;
            _printerStore = printerStore;
        }
        protected override async Task ExecuteAsync(object parameter)
        {
            try
            {
                await _printerStore.PrintLabel(_sumStore.CurrentMeasurement, _sumStore.Barcode, _sumStore.CustomerNumber);

                _messageStore.UpdateMessage(new Message(Messages.ProcessFinished, MessageColors.Green, false));
            }
            catch (PrintLabelFailedException e)
            {
                Logger.ErrorEx(e.Message, SourceClass, e);
                _messageStore.UpdateMessage(new Message(Messages.ErrorWhilePrintingLabel, MessageColors.Red, false));
            }
            catch (Exception e)
            {
                Logger.ErrorEx(e.Message, SourceClass, e);
                _messageStore.UpdateMessage(new Message(Messages.ErrorWhilePrintingLabel, MessageColors.Red, false));
            }

        }
    }
}
